package nz.xbc.rubbishreminder.collection

import org.joda.time.DateTime

data class CollectionDate(val weekId: Int,
                          val date: DateTime,
                          val includeRubbish: Boolean,
                          val includeRecycling: Boolean,
                          val isConfirmed: Boolean)
