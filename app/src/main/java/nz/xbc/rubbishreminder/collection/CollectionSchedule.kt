package nz.xbc.rubbishreminder.collection

import nz.xbc.rubbishreminder.common.Day
import org.joda.time.DateTime

data class CollectionSchedule(val id: Long,
                              val day: Day,
                              val hasRecycling: Boolean,
                              val hasRubbish: Boolean,
                              val recyclingDate: DateTime?,
                              val addressKey: String?)
