package nz.xbc.rubbishreminder.collection

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import nz.xbc.rubbishreminder.common.AppSharedPrefs
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.common.WeekIds.convertToWeekId
import nz.xbc.rubbishreminder.db.CollectionDateEntity
import nz.xbc.rubbishreminder.db.CollectionScheduleDao
import nz.xbc.rubbishreminder.db.CollectionScheduleEntity
import nz.xbc.rubbishreminder.db.CurrentScheduleEntity
import nz.xbc.rubbishreminder.rubbishtimes.CollectionDateResponse
import nz.xbc.rubbishreminder.rubbishtimes.RubbishTimesResponse
import nz.xbc.rubbishreminder.rubbishtimes.RubbishTimesService
import nz.xbc.rubbishreminder.util.associateByMulti
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CollectionRepo @Inject constructor(private var service: RubbishTimesService,
                                         private val schedulesDao: CollectionScheduleDao,
                                         private val sharedPrefs: AppSharedPrefs) {

    companion object {
        private const val TAG = "CollectionRepo"
    }

    fun observeSchedule(): Observable<CollectionSchedule?> = schedulesDao.observeCurrentSchedule()
            .map { scheduleEntity ->
                scheduleEntity.mapToCollectionSchedule()
            }.toObservable()

    fun observeCollectionDates(): Observable<List<CollectionDate>> =
            schedulesDao.observeCurrentCollectionDates()
                    .map { collectionDateEntities ->
                        collectionDateEntities.map { entity -> entity.mapToCollectionDate() }
                    }.toObservable()

    fun hasScheduleAvailable(): Boolean {
        return sharedPrefs.getHasSchedule();
    }

    fun chooseAddress(addressKey: String): Single<CollectionSchedule> =
            fetchCollectionSchedule(addressKey)
                    .doOnSuccess {
                        schedulesDao.insertCurrentSchedule(CurrentScheduleEntity(it.id))
                    }


    fun fetchCollectionSchedule(addressKey: String): Single<CollectionSchedule> {
        return service.fetchAddressSchedule(addressKey).map { response ->
            val scheduleEntity = response.mapToEntity(addressKey)
            val scheduleId = updateSchedule(scheduleEntity)
            schedulesDao.insertCollectionDates(response.dates.map { it.mapToEntity(scheduleId) })
            schedulesDao.setScheduleAddress(addressKey, scheduleId)
            sharedPrefs.setHasSchedule(true)
            addScheduledCollectionDates()
            schedulesDao.loadSchedule(scheduleId)!!.mapToCollectionSchedule()
        }.subscribeOn(Schedulers.io())
    }

    private fun updateSchedule(scheduleEntity: CollectionScheduleEntity): Long {
        val addressKey = scheduleEntity.addressKey
        // If a schedule with this addressKey already exists, use that id.
        // TODO: This whole dance could probably be done in a single SQLite query
        if (addressKey != null) {
            val currentSchedule = schedulesDao.loadScheduleWithAddress(addressKey)
            if (currentSchedule != null) {
                scheduleEntity.scheduleId = currentSchedule.scheduleId
            }
        }

        return schedulesDao.insertSchedule(scheduleEntity)
    }

    fun saveSchedule(day: Day, hasRecycling: Boolean, hasRubbish: Boolean, recyclingDate: DateTime): Completable {
        return Completable.fromAction {
            val id = schedulesDao.insertSchedule(CollectionScheduleEntity(addressKey = null,
                    day = day.constant,
                    hasRecycling = hasRecycling,
                    hasRubbish = hasRubbish,
                    recyclingDate = recyclingDate.millis))
            Timber.d("inserting current schedule with id=$id")
            schedulesDao.insertCurrentSchedule(CurrentScheduleEntity(id))
            sharedPrefs.setHasSchedule(true)
        }.subscribeOn(Schedulers.io())
    }

    fun getCollectionDates(): List<CollectionDate> {
        return schedulesDao.getCollectionDates().filterNotNull().map { it.mapToCollectionDate() }
    }

    fun removeOutdatedCollectionDates(): Completable {
        return Completable.fromAction {
            schedulesDao.clearCollectionTimesBefore(DateTime.now().millis)
        }.subscribeOn(Schedulers.io())
    }

    private fun addScheduledCollectionDates() {
        val dateMap = schedulesDao.getCollectionDates().filterNotNull()
                .associateByMulti { it.scheduleId }
        for ((scheduleId, collectionDateEntities) in dateMap) {
            val confirmedDates = collectionDateEntities.filter { it.isConfirmed }
            val datesToGenerate = 5 - confirmedDates.size
            if (datesToGenerate > 0) {
                val generatedDates = CollectionDateGenerator.generateDates(datesToGenerate,
                        confirmedDates.last().mapToCollectionDate(),
                        schedulesDao.loadSchedule(scheduleId)!!.mapToCollectionSchedule())
                schedulesDao.insertCollectionDates(generatedDates.map { it.mapToEntity(scheduleId) })
            }
        }
    }


    private fun RubbishTimesResponse.mapToEntity(key: String): CollectionScheduleEntity {
        val entity = CollectionScheduleEntity()
        entity.addressKey = key
        entity.day = rubbishCollectionDay?.constant ?: recyclingCollectionDay?.constant ?: throw IllegalStateException("No collection day in response")
        entity.recyclingDate = dates.find({ it.hasRecycling })?.date?.millis ?: -1
        entity.hasRecycling = recyclingCollectionDay != null
        entity.hasRubbish = rubbishCollectionDay != null
        return entity
    }

    private fun CollectionDateResponse.mapToEntity(scheduleId: Long): CollectionDateEntity {
        return CollectionDateEntity(
                weekId = convertToWeekId(date),
                scheduleId = scheduleId,
                date = date.millis,
                includesRubbish = hasRubbish,
                includesRecycling = hasRecycling,
                isConfirmed = true
        )
    }

    private fun CollectionDate.mapToEntity(scheduleId: Long): CollectionDateEntity {
        return CollectionDateEntity(
                weekId = weekId,
                scheduleId = scheduleId,
                date = date.millis,
                includesRubbish = includeRubbish,
                includesRecycling = includeRecycling,
                isConfirmed = isConfirmed
        )
    }

    private fun CollectionDateEntity.mapToCollectionDate(): CollectionDate {
        return CollectionDate(
                weekId,
                DateTime(date),
                includesRubbish,
                includesRecycling,
                isConfirmed)
    }

    private fun CollectionScheduleEntity.mapToCollectionSchedule(): CollectionSchedule {
        return CollectionSchedule(
                id = scheduleId!!,
                addressKey = addressKey,
                day = Day.fromInt(day) ?: throw IllegalArgumentException("Invalid day: $day"),
                hasRecycling = hasRecycling,
                hasRubbish = hasRubbish,
                recyclingDate = DateTime(recyclingDate)
        )
    }

}

