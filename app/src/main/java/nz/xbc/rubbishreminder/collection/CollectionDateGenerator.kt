package nz.xbc.rubbishreminder.collection

object CollectionDateGenerator {

    fun generateDates(count: Int,
                      oldest: CollectionDate,
                      collectionSchedule: CollectionSchedule): List<CollectionDate> {
        val generated = mutableListOf<CollectionDate>()

        var currOldest = oldest
        for (i in 0 until count) {
            currOldest = CollectionDate(
                    weekId = currOldest.weekId + 1,
                    date = currOldest.date.plusWeeks(1).withDayOfWeek(collectionSchedule.day.constant),
                    includeRecycling = if (collectionSchedule.hasRecycling) !currOldest.includeRecycling else false,
                    includeRubbish = collectionSchedule.hasRubbish,
                    isConfirmed = false)
            generated.add(currOldest)
        }
        return generated
    }
}
