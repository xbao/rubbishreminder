package nz.xbc.rubbishreminder

import android.app.Application
import com.facebook.stetho.Stetho
import net.danlew.android.joda.JodaTimeAndroid
import nz.xbc.rubbishreminder.alarm.ReminderManager
import nz.xbc.rubbishreminder.common.di.AppComponent
import nz.xbc.rubbishreminder.common.di.AppModule
import nz.xbc.rubbishreminder.common.di.DaggerAppComponent
import nz.xbc.rubbishreminder.common.network.RetrofitModule
import nz.xbc.rubbishreminder.db.AppDatabase
import nz.xbc.rubbishreminder.db.CollectionScheduleDao
import nz.xbc.rubbishreminder.db.DbModule
import timber.log.Timber
import javax.inject.Inject
import kotlin.properties.Delegates


open class App : Application() {

    @Inject
    lateinit var reminderManager: ReminderManager

    @Inject
    lateinit var schedulesDao: CollectionScheduleDao

    var appComponent: AppComponent by Delegates.notNull()

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
        Timber.plant(Timber.DebugTree())

        appComponent = createAppComponent()
        appComponent.inject(this)

        reminderManager.startObservingDates()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    protected open fun setupTimber(appComponent: AppComponent) {
        Timber.plant(appComponent.logPersister().createTimberTree())
    }

    protected open fun createAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .retrofitModule(RetrofitModule())
                .dbModule(DbModule(AppDatabase.create(this)))
                .build()
    }

}
