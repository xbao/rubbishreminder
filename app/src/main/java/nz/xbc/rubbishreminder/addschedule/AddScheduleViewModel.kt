package nz.xbc.rubbishreminder.addschedule

import androidx.lifecycle.ViewModel
import io.reactivex.subjects.BehaviorSubject
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.common.daysUntilClosestDayOfWeek
import nz.xbc.rubbishreminder.common.withNextDayOfWeek
import nz.xbc.rubbishreminder.common.withPreviousDayOfWeek
import nz.xbc.rubbishreminder.collection.CollectionRepo
import org.joda.time.DateTime
import javax.inject.Inject

class AddScheduleViewModel @Inject constructor(private val collectionRepo: CollectionRepo) : ViewModel() {

    val observeViewData = BehaviorSubject.createDefault<ViewData>(ViewData(Stage.PICK_DAY, false, emptyList()))
    var dayPicked: Day? = null

    fun dayPicked(picked: Day) {
        dayPicked = picked;
        updateView {
            it.copy(stage = Stage.PICK_RECYCLING_DAY,
                    isCheckingOnline = true,
                    recyclingOptions = calculateRecyclingOptions(picked, DateTime.now()))
        }
    }

    fun recyclingOptionPicked(option: RecyclingOption) {
        collectionRepo.saveSchedule(dayPicked!!, true, true, option.date)
                .subscribe {
                    updateView {
                        it.copy(stage = Stage.DONE)
                    }
                }
    }

    fun calculateRecyclingOptions(pickedDay: Day, currentDate: DateTime): List<RecyclingOption> {
        val daysDiff = currentDate.daysUntilClosestDayOfWeek(pickedDay.constant)
        if (daysDiff == 0) {
            // Same day! Options are last week and today
            return listOf(
                    RecyclingOption(RelativeDate.Last(pickedDay), currentDate.minusDays(7)),
                    RecyclingOption(RelativeDate.Today, currentDate)
            )
        }
        if (daysDiff == 1) {
            if(currentDate.plusDays(1).dayOfWeek == pickedDay.constant) {
                // tomorrow
                return listOf(
                        RecyclingOption(RelativeDate.Last(pickedDay), currentDate.minusDays(6)),
                        RecyclingOption(RelativeDate.Tomorrow, currentDate.plusDays(1))
                )
            } else {
                // yesterday
                return listOf(
                        RecyclingOption(RelativeDate.Yesterday, currentDate.minusDays(1)),
                        RecyclingOption(RelativeDate.Next(pickedDay), currentDate.plusDays(6))
                )
            }
        }
        return listOf(
                RecyclingOption(RelativeDate.Last(pickedDay), currentDate.withPreviousDayOfWeek(pickedDay.constant)),
                RecyclingOption(RelativeDate.Next(pickedDay), currentDate.withNextDayOfWeek(pickedDay.constant))
        )
    }

    fun updateView(mutator: (ViewData) -> ViewData) {
        observeViewData.onNext(mutator.invoke(observeViewData.value))
    }


    data class ViewData(val stage: Stage,
                        val isCheckingOnline: Boolean,
                        val recyclingOptions: List<RecyclingOption>)

    enum class Stage {
        PICK_DAY,
        PICK_RECYCLING_DAY,
        DONE
    }

    sealed class RelativeDate {
        object Today : RelativeDate()
        object Tomorrow : RelativeDate()
        object Yesterday : RelativeDate()
        data class Last(val day: Day) : RelativeDate()
        data class Next(val day: Day) : RelativeDate()
    }

    data class RecyclingOption(val relativeDate: RelativeDate, val date: DateTime)

}
