package nz.xbc.rubbishreminder.addschedule

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.add_schedule_activity.*
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.addressselection.ChooseAddressActivity
import nz.xbc.rubbishreminder.common.BaseActivity
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.main.MainActivity
import nz.xbc.rubbishreminder.util.inflate
import kotlin.properties.Delegates

class AddScheduleActivity : BaseActivity() {

    private var viewModel: AddScheduleViewModel by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.add_schedule_activity)

        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return appComponent.addScheduleViewModel() as T
            }

        }).get(AddScheduleViewModel::class.java)

        set_address_view.setOnClickListener { v ->
            startActivity(Intent(this, ChooseAddressActivity::class.java))
        }

        for (day in Day.values()) {
            inflate<Button>(R.layout.add_schedule_day_button, pick_day_layout).apply {
                text = day.displayName
                setOnClickListener {
                    viewModel.dayPicked(day)
                }
                pick_day_layout.addView(this)
            }
        }

        viewModel.observeViewData
                .subscribe { viewData ->
                    when (viewData.stage) {
                        AddScheduleViewModel.Stage.PICK_DAY -> {
                            pick_day_layout.visibility = View.VISIBLE
                            pick_recycling_day_layout.visibility = View.GONE

                        }
                        AddScheduleViewModel.Stage.PICK_RECYCLING_DAY -> {
                            pick_day_layout.visibility = View.GONE
                            pick_recycling_day_layout.visibility = View.VISIBLE
                            for (option in viewData.recyclingOptions) {
                                inflate<Button>(R.layout.add_schedule_day_button, pick_recycling_day_layout).apply {
                                    text = option.relativeDate.toDisplayString()
                                    setOnClickListener {
                                        viewModel.recyclingOptionPicked(option)
                                    }
                                    pick_recycling_day_layout.addView(this)
                                }
                            }

                        }
                        AddScheduleViewModel.Stage.DONE -> {
                            startActivity(Intent(this, MainActivity::class.java))
                        }
                    }
                }
    }

    fun AddScheduleViewModel.RelativeDate.toDisplayString(): String {
        when (this) {
            is AddScheduleViewModel.RelativeDate.Tomorrow -> {
                return "Tomorrow"
            }
            is AddScheduleViewModel.RelativeDate.Yesterday -> {
                return "Yesterday"
            }
            is AddScheduleViewModel.RelativeDate.Next -> {
                return "Next ${this.day.displayName}"
            }
            is AddScheduleViewModel.RelativeDate.Last -> {
                return "Last ${this.day.displayName}"
            }
            is AddScheduleViewModel.RelativeDate.Today -> {
                return "Today"
            }
        }
    }

}
