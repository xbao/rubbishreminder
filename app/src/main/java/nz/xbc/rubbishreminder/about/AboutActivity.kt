package nz.xbc.rubbishreminder.about

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.common.di.appComponent
import nz.xbc.rubbishreminder.util.inflate


class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val container = FrameLayout(this)
        container.id = R.id.fragment_container
        setContentView(container,
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
        )
        if (supportFragmentManager.findFragmentById(R.id.about_fragment) == null && savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, AboutFragment())
                .commitNow()
        }
    }
}

class AboutFragment : androidx.fragment.app.Fragment() {

    val adapter: LogRecyclerAdapter = LogRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context!!.appComponent().logPersister().dao.observeLogs().observe(this, Observer {
            adapter.updateLogs(it?.map {
                "${it.date?.toString("EEE ddMM HH:mm")} | ${it.log}"
            } ?: emptyList())
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.about_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = adapter
    }
}

class LogViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val textView: TextView = view.findViewById<TextView>(R.id.log_text)
}

class LogRecyclerAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<LogViewHolder>() {
    private var logs = emptyList<String>()

    fun updateLogs(logs: List<String>) {
        this.logs = logs
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogViewHolder =
        LogViewHolder(parent.inflate(R.layout.log_item))

    override fun getItemCount(): Int = logs.size

    override fun onBindViewHolder(holder: LogViewHolder, position: Int) {
        holder.textView.text = logs[position]
    }
}
