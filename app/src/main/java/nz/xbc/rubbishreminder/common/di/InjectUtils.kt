package nz.xbc.rubbishreminder.common.di

import android.content.Context
import nz.xbc.rubbishreminder.App

fun Context.appComponent() = (applicationContext as App).appComponent
