package nz.xbc.rubbishreminder.common.di

import javax.inject.Scope

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class AppScope
