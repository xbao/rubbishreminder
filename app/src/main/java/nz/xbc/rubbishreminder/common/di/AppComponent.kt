package nz.xbc.rubbishreminder.common.di

import dagger.Component
import nz.xbc.rubbishreminder.App
import nz.xbc.rubbishreminder.addressselection.ChooseAddressViewModel
import nz.xbc.rubbishreminder.addschedule.AddScheduleViewModel
import nz.xbc.rubbishreminder.alarm.NotificationReceiver
import nz.xbc.rubbishreminder.alarm.ReminderManager
import nz.xbc.rubbishreminder.common.log.LogPersister
import nz.xbc.rubbishreminder.main.MainActivity
import nz.xbc.rubbishreminder.main.MainViewModel
import javax.inject.Singleton

@Component(modules = [(AppModule::class)])
@AppScope
@Singleton
interface AppComponent {
    fun inject(app: App)
    fun inject(app: MainActivity)
    fun mainViewModel(): MainViewModel
    fun chooseAddressViewModel(): ChooseAddressViewModel
    fun addScheduleViewModel(): AddScheduleViewModel
    fun inject(notificationReceiver: NotificationReceiver)

    fun reminderManager(): ReminderManager
    fun logPersister(): LogPersister
}
