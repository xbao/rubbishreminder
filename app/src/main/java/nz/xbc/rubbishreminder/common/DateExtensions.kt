package nz.xbc.rubbishreminder.common

import org.joda.time.DateTime
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun DateTime.withPreviousDayOfWeek(targetDay: Int): DateTime {
    if (dayOfWeek > targetDay) {
        return withDayOfWeek(targetDay)
    }
    return minusWeeks(1).withDayOfWeek(targetDay)
}

fun DateTime.withNextDayOfWeek(targetDay: Int): DateTime {
    if (dayOfWeek < targetDay) {
        return withDayOfWeek(targetDay)
    }
    return plusWeeks(1).withDayOfWeek(targetDay)
}

fun DateTime.daysUntilClosestDayOfWeek(targetDay: Int): Int {
    if (dayOfWeek == targetDay) {
        return 0
    }
    val dayDiff = abs(dayOfWeek - targetDay)
    if (dayDiff > 3) {
        return (min(dayOfWeek, targetDay) + 7) - max(dayOfWeek, targetDay)
    }
    return dayDiff
}
