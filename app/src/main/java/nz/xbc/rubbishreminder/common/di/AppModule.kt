package nz.xbc.rubbishreminder.common.di

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import nz.xbc.rubbishreminder.common.Clock
import nz.xbc.rubbishreminder.common.log.LogPersister
import nz.xbc.rubbishreminder.common.network.RetrofitModule
import nz.xbc.rubbishreminder.db.DbModule
import javax.inject.Singleton

@Module(includes = [
    RetrofitModule::class,
    DbModule::class])
class AppModule(private val applicationContext: Context) {

    @Provides
    @Singleton
    fun provideContext() = applicationContext

    @Provides
    @Singleton
    fun provideMainSharedPreferences() = PreferenceManager.getDefaultSharedPreferences(applicationContext)

    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    fun provideResources() = applicationContext.resources

    @Provides
    @Singleton
    fun provideClock() = Clock.default()

    @Provides
    @Singleton
    fun logPersister() = LogPersister(applicationContext)
}
