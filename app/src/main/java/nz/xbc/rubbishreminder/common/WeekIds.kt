package nz.xbc.rubbishreminder.common

import nz.xbc.rubbishreminder.util.Dates
import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.PeriodType


object WeekIds {

    private val WEEK_ZERO = Dates.easyDate("Mon 02 Jan 2017")

    /**
     * The week id is the number of weeks between starting the first week of 2017. A week starts on
     * Monday and ends on Sunday. Week id 0 is Monday 2nd January 2017 to Sunday 8th January 2018
     */
    fun convertToWeekId(date: DateTime) : Int {
        return Interval(WEEK_ZERO, date).toPeriod(PeriodType.weeks()).weeks
    }

    fun convertToDateTime(weekId: Int) : DateTime {
        return WEEK_ZERO.plusWeeks(weekId)
    }
}
