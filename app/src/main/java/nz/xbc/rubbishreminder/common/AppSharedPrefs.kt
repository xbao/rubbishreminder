package nz.xbc.rubbishreminder.common

import android.content.SharedPreferences
import nz.xbc.rubbishreminder.common.AppSharedPrefs.KEYS.Companion.HAS_SCHEDULE
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppSharedPrefs @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private class KEYS {
        companion object {
            val HAS_SCHEDULE = "HAS_SCHEDULE";
        }
    }

    fun setHasSchedule(hasSchedule: Boolean) {
        sharedPreferences.edit()
                .putBoolean(HAS_SCHEDULE, hasSchedule)
                .apply()
    }

    fun getHasSchedule(): Boolean {
        return sharedPreferences.getBoolean(HAS_SCHEDULE, false);
    }

}
