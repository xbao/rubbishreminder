package nz.xbc.rubbishreminder.common

import org.joda.time.DateTime

open class Clock(private val nowProvider: () -> DateTime) {
    companion object {
        fun default() = Clock { DateTime.now() }
    }

    fun now(): DateTime = nowProvider.invoke()
}


