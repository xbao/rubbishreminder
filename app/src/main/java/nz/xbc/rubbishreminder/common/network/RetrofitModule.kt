package nz.xbc.rubbishreminder.common.network

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import nz.xbc.rubbishreminder.BuildConfig
import nz.xbc.rubbishreminder.addressselection.AddressSuggestionService
import nz.xbc.rubbishreminder.rubbishtimes.RubbishTimesParser
import nz.xbc.rubbishreminder.rubbishtimes.RubbishTimesService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
open class RetrofitModule {

    @Provides
    @Singleton
    fun createRetrofit(): Retrofit {
        return DEFAULT_BUILDER
                .client(createOkHttpClient())
                .build()
    }

    @Provides
    @Singleton
    fun provideRubbishService(retrofit: Retrofit, rubbishTimesParser: RubbishTimesParser): RubbishTimesService {
        return RubbishTimesService(
                restService = retrofit.create(RubbishTimesService.RubbishTimesRestService::class.java),
                rubbishTimesParser = rubbishTimesParser)
    }

    @Provides
    @Singleton
    fun provideAddressSuggestionService(retrofit: Retrofit): AddressSuggestionService {
        return retrofit.create(AddressSuggestionService::class.java)
    }

    protected fun createOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .setLogging()
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }

    private fun OkHttpClient.Builder.setLogging(): OkHttpClient.Builder {
        if (Log.isLoggable(PROP_LOG_TAG, Log.VERBOSE)) {
            return addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        if (Log.isLoggable(PROP_LOG_TAG, Log.DEBUG)) {
            return addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS))
        }
        return if (BuildConfig.DEBUG || Log.isLoggable(PROP_LOG_TAG, Log.INFO)) {
            addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
        } else this
    }


    companion object {

        private val DEFAULT_BUILDER = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl("http://thisisnotused.com")

        /**
         * Logging tag. To log headers:
         *
         * `adb shell setprop log.tag.RubbishReminder DEBUG`
         *
         * or to log the full body:
         *
         * `adb shell setprop log.tag.RubbishReminder VERBOSE`
         *
         * then restart the app.
         *
         * By default debug builds log the header, non-debug builds do no logging
         */
        private val PROP_LOG_TAG = "RubbishReminder"

    }
}
