package nz.xbc.rubbishreminder.common

import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import nz.xbc.rubbishreminder.App
import nz.xbc.rubbishreminder.common.di.AppComponent

abstract class BaseActivity : AppCompatActivity() {

    protected val onPauseDisposables = CompositeDisposable()
    protected val onDestroyDisposables = CompositeDisposable()

    protected val appComponent: AppComponent
        get() = (application as App).appComponent

    override fun onPause() {
        super.onPause()
        onPauseDisposables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDestroyDisposables.clear()
    }
}
