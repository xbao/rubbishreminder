package nz.xbc.rubbishreminder.common


data class Result<T>(
        val data: T? = null,
        val error: Throwable? = null,
        val isLoading: Boolean = false) {

    fun withLoading(loading: Boolean): Result<T> {
        return copy(isLoading = loading, error = null)
    }

    fun withError(error: Throwable?): Result<T> {
        return copy(isLoading = false, error = error)
    }

    fun success(data: T): Result<T> {
        return Result(data)
    }
}


sealed class Result2<T>(open val isLoading: Boolean) {
    class Empty<T> : Result2<T>(false)
    class Success<T>(val data: T, isLoading: Boolean, val isCached: Boolean = false) : Result2<T>(isLoading)
    class Error<T>(val t: Throwable, isLoading: Boolean) : Result2<T>(isLoading)
}
