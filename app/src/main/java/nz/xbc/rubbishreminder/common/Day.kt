package nz.xbc.rubbishreminder.common

import org.joda.time.DateTime
import org.joda.time.DateTimeConstants

enum class Day(val constant: Int, val displayName: String) {
    MONDAY(DateTimeConstants.MONDAY, "Monday"),
    TUESDAY(DateTimeConstants.TUESDAY, "Tuesday"),
    WEDNESDAY(DateTimeConstants.WEDNESDAY, "Wednesday"),
    THURSDAY(DateTimeConstants.THURSDAY, "Thursday"),
    FRIDAY(DateTimeConstants.FRIDAY, "Friday"),
    SATURDAY(DateTimeConstants.SATURDAY, "Saturday"),
    SUNDAY(DateTimeConstants.SUNDAY, "Sunday");

    companion object {
        val DAY_CONSTANT_MAP: Map<Int, Day> = mapOf(*(values().map { it.constant to it }.toTypedArray()))

        private val dayStringMap = hashMapOf(
                "monday" to Day.MONDAY,
                "tuesday" to Day.TUESDAY,
                "wednesday" to Day.WEDNESDAY,
                "thursday" to Day.THURSDAY,
                "friday" to Day.FRIDAY,
                "saturday" to Day.SATURDAY,
                "sunday" to Day.SUNDAY
        )

        fun fromDate(date: DateTime): Day {
            return fromInt(date.dayOfWeek)!!
        }
        fun fromText(text: String): Day? {
            return dayStringMap[text.toLowerCase()]
        }

        fun fromInt(constant: Int): Day? {
            return DAY_CONSTANT_MAP[constant]
        }
    }
}
