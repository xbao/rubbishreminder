package nz.xbc.rubbishreminder.common.log

import androidx.lifecycle.LiveData
import androidx.room.*
import android.content.Context
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import timber.log.Timber
import kotlin.concurrent.getOrSet

@Entity(tableName = "logs")
data class LogEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var date: DateTime? = null,
    var thread: Long = 0,
    var log: String = ""
)

object DateTimeConverter {
    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: Long?): DateTime? = if (value == null) null else DateTime(value)

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: DateTime?): Long? = date?.millis
}

@Database(entities = [LogEntity::class], version = 1)
@TypeConverters(DateTimeConverter::class)
abstract class LogDatabase : RoomDatabase() {
    companion object {
        fun create(context: Context) =
            Room.databaseBuilder(context, LogDatabase::class.java, "log-db")
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun logDao(): LogDao
}

@Dao
abstract class LogDao {
    @Query("SELECT * FROM logs ORDER BY date DESC")
    abstract fun queryLogs(): List<LogEntity>
    @Query("SELECT * FROM logs ORDER BY date DESC")
    abstract fun observeLogs(): LiveData<List<LogEntity>>

    @Insert
    abstract fun insertLog(entity: LogEntity)
}


class LogPersister(context: Context) {
    val dao = LogDatabase.create(context).logDao()
    val entityTl = ThreadLocal<LogEntity>()
    fun writeLog(message: String) {
        val threadId = Thread.currentThread().id
        val logDate = DateTime.now()
        Schedulers.io().scheduleDirect {
            dao.insertLog(
                entityTl.getOrSet { LogEntity() }.apply {
                    thread = threadId
                    log = message
                    date = logDate
                })
        }
    }

    fun createTimberTree(): Timber.Tree {
        return object : Timber.Tree() {
            override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                if(t == null) {
                    writeLog("[$tag] $message")
                } else {
                    writeLog("[$tag] $message _error_=$t")
                }
            }
        }
    }
}
