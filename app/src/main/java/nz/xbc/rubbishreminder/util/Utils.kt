package nz.xbc.rubbishreminder.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.subjects.BehaviorSubject

@Suppress("UNCHECKED_CAST")
fun <T> Context.inflate(layout: Int, parent: ViewGroup, attachToParent: Boolean = false): T {
    return LayoutInflater.from(this).inflate(layout, parent, attachToParent) as T
}

fun ViewGroup.inflate(layout: Int, attachToParent: Boolean = false): View {
    return context.inflate(layout, this, attachToParent)
}

fun View.visibleIf(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}


class ObserverHelper<T>(val data: LiveData<T>, val observer: Observer<T>) {
    // Can't use MediatorLiveData as it doesn't propagate initial values
    init {
        data.observeForever(observer)
    }

    fun unobserve() {
        data.removeObserver(observer)
    }
}

fun <T> LiveData<T?>.observeManaged(observers: MutableList<ObserverHelper<*>>, observer: (T?) -> Unit) {
    observers.add(ObserverHelper(this, observer = Observer { observer.invoke(it) }))
}

fun <T> BehaviorSubject<T>.mutate(mutator: (T) -> T) {
    onNext(mutator.invoke(value))
}

fun AppCompatActivity.genericErrorDialog(throwable: Throwable) {
    AlertDialog.Builder(this)
            .setTitle(throwable.message)
            .setMessage(Log.getStackTraceString(throwable))
            .show()
}


fun <T, K> Collection<T>.associateByMulti(keySelector: (T) -> K): Map<K, List<T>> {
    return associateByToMulti(LinkedHashMap(size), keySelector)
}

fun <T, K, M : MutableMap<in K, MutableList<T>>> Collection<T>.associateByToMulti(destination: M, keySelector: (T) -> K): M {
    for (item in this) {
        val key = keySelector(item)
        val existingList = destination[key]
        if (existingList == null) {
            destination[key] = mutableListOf(item)
        } else {
            existingList.add(item)
        }
    }
    return destination
}
