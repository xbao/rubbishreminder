package nz.xbc.rubbishreminder.util

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object Dates {

    private val format = DateTimeFormat.forPattern("E dd MMM yyyy")
    private val formatTime = DateTimeFormat.forPattern("HH:mm E dd MMM yyyy")

    fun easyDate(date: String) : DateTime {
        return format.parseDateTime(date)
    }
    fun easyDateTime(date: String) : DateTime {
        return formatTime.parseDateTime(date)
    }
}
