package nz.xbc.rubbishreminder.util

import android.os.Handler
import android.os.HandlerThread

class BackgroundRunner {
    private val thread = HandlerThread("app-bg-thread")
    private val handler: Handler

    init {
        thread.start()
        handler = Handler(thread.looper)
    }

    fun post(runnable: () -> Unit) {
        handler.post(runnable)
    }
}