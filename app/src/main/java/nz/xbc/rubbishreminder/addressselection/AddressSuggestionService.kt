package nz.xbc.rubbishreminder.addressselection

import androidx.annotation.Keep
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

interface AddressSuggestionService {
    @POST
    fun getSuggestions(
        @Url url: String = "https://www.aucklandcouncil.govt.nz/_vti_bin/ACWeb/ACservices.svc/GetMatchingPropertyAddresses",
        @Body body: AddressSuggestionBody
    ): Call<List<AddressSuggestionResponse>>
}

@Keep
data class AddressSuggestionResponse(
    val ACRateAccountKey: String,
    val Address: String,
    val Suggestion: String
)

@Keep
data class AddressSuggestionBody(
    val SearchText: String,
    val ResultCount: Int = 10,
    val RateKeyRequired: String = "false"
)
