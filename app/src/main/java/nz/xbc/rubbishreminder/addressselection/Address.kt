package nz.xbc.rubbishreminder.addressselection


data class Address(val key: String, val address: String)