package nz.xbc.rubbishreminder.addressselection

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import nz.xbc.rubbishreminder.db.AddressEntity
import nz.xbc.rubbishreminder.db.AddressEntityDao
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AddressRepo @Inject constructor(val addressDao: AddressEntityDao) {

    fun getAddress(addressKey: String): Address? {
        return addressDao.loadAddress(addressKey)?.map()
    }

    fun saveAddress(address: Address): Completable {
        return Single.fromCallable({
            addressDao.insertAddress(address.mapToEntity())
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).toCompletable()
    }


    private fun Address.mapToEntity(): AddressEntity {
        return AddressEntity().apply {
            address = this@mapToEntity.address
            addressKey = this@mapToEntity.key
        }
    }

    private fun AddressEntity.map(): Address {
        return Address(key = addressKey,
                address = address)
    }
}
