package nz.xbc.rubbishreminder.addressselection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.choose_address_activity.*
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.collection.CollectionRepo
import nz.xbc.rubbishreminder.collection.CollectionSchedule
import nz.xbc.rubbishreminder.common.BaseActivity
import nz.xbc.rubbishreminder.common.Result
import nz.xbc.rubbishreminder.main.MainActivity
import nz.xbc.rubbishreminder.util.genericErrorDialog
import nz.xbc.rubbishreminder.util.inflate
import nz.xbc.rubbishreminder.util.mutate
import nz.xbc.rubbishreminder.util.visibleIf
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ChooseAddressActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_address_activity)
        val viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return appComponent.chooseAddressViewModel() as T
            }

        }).get(ChooseAddressViewModel::class.java)
        val inputView = findViewById<EditText>(R.id.editText)
        inputView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                viewModel.addressTextChanged(s.toString());
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        val suggestionsView = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recyclerView)
        val adapter = AddressSuggestionAdapter {
            viewModel.addressChosen(it)
        }
        suggestionsView.adapter = adapter

        viewModel.viewState
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { viewState ->
                    Timber.d(viewState.toString())
                    progress_text.visibleIf(viewState.scheduleResult.isLoading)
                    schedule_progress.visibleIf(viewState.scheduleResult.isLoading)
                    if (viewState.suggestions.error != null) {
                        genericErrorDialog(viewState.suggestions.error)
                    } else if (viewState.scheduleResult.data != null) {
                        startActivity(MainActivity.createIntent(this))
                    } else {
                        adapter.suggestions = viewState.suggestions.data ?: emptyList()
                        adapter.notifyDataSetChanged()
                    }
                }
    }
}


data class ChooseAddressViewState(
        val suggestions: Result<List<Address>>,
        val scheduleResult: Result<CollectionSchedule>)

class ChooseAddressViewModel @Inject constructor(private val addressRepo: AddressRepo,
                                                 private val suggestionService: AddressSuggestionService,
                                                 private val collectionRepo: CollectionRepo) : ViewModel() {


    companion object {
        val TAG = "ChooseAddressViewModel"
    }

    val viewState = BehaviorSubject.createDefault(ChooseAddressViewState(Result(), Result()))
    private var currentSuggestionCall: Call<List<AddressSuggestionResponse>>? = null

    val inputObservable = BehaviorSubject.createDefault("")

    init {
        inputObservable.debounce(1, TimeUnit.SECONDS).subscribe {
            addressSuggestionRefresh(it)
        }
    }

    fun addressSuggestionRefresh(searchText: String) {
        viewState.mutate { it.copy(suggestions = it.suggestions.withLoading(true)) }
        currentSuggestionCall?.cancel()
        currentSuggestionCall = suggestionService.getSuggestions(body = AddressSuggestionBody(SearchText = searchText))
        currentSuggestionCall!!.enqueue(object : Callback<List<AddressSuggestionResponse>> {
            override fun onFailure(call: Call<List<AddressSuggestionResponse>>, t: Throwable) {
                viewState.mutate { it.copy(suggestions = it.suggestions.withLoading(false)) }
            }

            override fun onResponse(call: Call<List<AddressSuggestionResponse>>, response: Response<List<AddressSuggestionResponse>>) {
                viewState.mutate {
                    it.copy(suggestions = it.suggestions.copy(
                            data = response.body()!!.map {
                                Address(address = it.Suggestion,
                                        key = it.ACRateAccountKey)
                            },
                            isLoading = false
                    ));
                }
            }

        })
    }

    fun addressChosen(address: Address) {

        viewState.mutate { it.copy(scheduleResult = it.scheduleResult.withLoading(true)) }

        addressRepo.saveAddress(address)
                .andThen(collectionRepo.chooseAddress(address.key))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { schedule ->
                    viewState.mutate { it.copy(scheduleResult = it.scheduleResult.success(schedule)) }
                }

    }

    fun addressTextChanged(newText: String) {
        inputObservable.onNext(newText)
    }

}


class AddressSuggestionViewHolder(view: View, clickListener: (Address) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val addressView = view.findViewById<TextView>(R.id.textView)
    var suggestion: Address? = null

    init {
        view.setOnClickListener {
            suggestion?.let {
                clickListener.invoke(it)
            }
        }
    }

    fun bind(suggestion: Address) {
        this.suggestion = suggestion
        addressView.text = suggestion.address
    }
}

class AddressSuggestionAdapter(val clickListener: (Address) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<AddressSuggestionViewHolder>() {
    var suggestions: List<Address> = emptyList()
    override fun getItemCount(): Int {
        return suggestions.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressSuggestionViewHolder {
        return AddressSuggestionViewHolder(parent.inflate(R.layout.address_suggestion_item), clickListener)
    }

    override fun onBindViewHolder(holder: AddressSuggestionViewHolder, position: Int) {
        holder.bind(suggestions[position])
    }

}
