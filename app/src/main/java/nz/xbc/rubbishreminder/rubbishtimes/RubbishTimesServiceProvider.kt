package nz.xbc.rubbishreminder.rubbishtimes

import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Provider

class RubbishTimesServiceProvider(private val rubbishTimesParser: RubbishTimesParser) : Provider<RubbishTimesService> {
    override fun get(): RubbishTimesService {
        return RubbishTimesService(Retrofit.Builder().baseUrl("https://www.aucklandcouncil.govt.nz")
                .client(OkHttpClient.Builder()
                        .addNetworkInterceptor(HttpLoggingInterceptor()
                                .setLevel(HttpLoggingInterceptor.Level.BASIC))
                        .addNetworkInterceptor(StethoInterceptor())
                        .build())
                .build()
                .create(RubbishTimesService.RubbishTimesRestService::class.java), rubbishTimesParser)
    }
}
