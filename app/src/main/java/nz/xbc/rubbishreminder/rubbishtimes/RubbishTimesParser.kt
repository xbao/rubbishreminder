package nz.xbc.rubbishreminder.rubbishtimes

import nz.xbc.rubbishreminder.common.Clock
import nz.xbc.rubbishreminder.common.Day
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import timber.log.Timber
import java.util.regex.Pattern
import javax.inject.Inject

data class RubbishTimesResponse(val rubbishCollectionDay: Day?,
                                val rubbishCollectionInterval: Interval?,
                                val recyclingCollectionDay: Day?,
                                val recyclingCollectionInterval: Interval?,
                                val dates: List<CollectionDateResponse>)

class RubbishTimesParser @Inject constructor(private val clock: Clock) {
    private val TAG = "RubbishTimesParser"
    private val COLLECTION_DATE_FORMATTER = DateTimeFormat.forPattern("EEEE d MMMM")
    private val COLLECTION_DATE_FORMATTER_WITH_YEAR = DateTimeFormat.forPattern("EEEE d MMMM yyyy")

    fun parseResponse(response: String): RubbishTimesResponse {
        val document = Jsoup.parse(response)
        val cardElements = document.select(".card-content")
        if (cardElements.isEmpty()) {
            throw ParseException("No elements with class .card-content found")
        }
        // second one is commercial
        val domesticCardElement = cardElements[0]

        val schedules = parseCollectionScheduleFromCardHeader(domesticCardElement.select(".card-header")[0])
        val dates = parseCollectionDatesFromCardBlock(domesticCardElement.select(".card-block")[0])

        val rubbishSchedule = schedules.first
        val recyclingSchedule = schedules.second
        return RubbishTimesResponse(rubbishCollectionDay = rubbishSchedule?.day,
                rubbishCollectionInterval = rubbishSchedule?.interval,
                recyclingCollectionDay = recyclingSchedule?.day,
                recyclingCollectionInterval = recyclingSchedule?.interval,
                dates = dates)
    }

    private fun parseCollectionDatesFromCardBlock(element: Element): List<CollectionDateResponse> {
        val dates = mutableListOf<CollectionDateResponse>()
        for (child in element.children()) {
            if (child.select(".sr-only").isEmpty()) {
                continue
            }
            val dateText = child.select("div span.m-r-1")[0].text()
            val hasRubbish = child.select("div span.icon-rubbish").isNotEmpty()
            val hasRecycling = child.select("div span.icon-recycle").isNotEmpty()
            dates.add(CollectionDateResponse(
                    date = calculateDate(dateText),
                    hasRecycling = hasRecycling,
                    hasRubbish = hasRubbish
            ))
        }
        return dates
    }

    private fun calculateDate(dateText: String?): DateTime {
        val rawDate = COLLECTION_DATE_FORMATTER.parseDateTime(dateText)
        val year = clock.now().year
        val dateTextWithYear = if (rawDate.monthOfYear < clock.now().monthOfYear) {
            "$dateText ${year + 1}"
        } else {
            "$dateText $year"
        }
        return COLLECTION_DATE_FORMATTER_WITH_YEAR.parseDateTime(dateTextWithYear)
    }

    fun parseCollectionScheduleFromCardHeader(cardHeader: Element): Pair<ScheduleResponse?, ScheduleResponse?> {
        var rubbishSchedule: ScheduleResponse? = null
        var recyclingSchedule: ScheduleResponse? = null
        for ((index, child) in cardHeader.children().withIndex()) {
            if (child.`is`("h4")) {
                when {
                    child.text().contains("Rubbish") -> {
                        rubbishSchedule = parseCollectionScheduleText(cardHeader.child(index + 1).text())
                    }
                    child.text().contains("Recycling") -> {
                        recyclingSchedule = parseCollectionScheduleText(cardHeader.child(index + 1).text())
                    }
                    else -> Timber.w("Unknown h4 element: ${child.text()}")
                }
            }
        }

        return Pair(rubbishSchedule, recyclingSchedule)

    }

    fun parseCollectionScheduleText(text: String): ScheduleResponse? {
        val pattern = Pattern.compile(" (Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), (weekly|fortnightly)")
        val matcher = pattern.matcher(text)
        if (matcher.groupCount() != 2 || !matcher.find()) {
            return null
        }

        val day = Day.fromText(matcher.group(1)) ?: throw ParseException("No match group for day. text=$text")

        val interval = Interval.fromText(matcher.group(2))
                ?: throw ParseException("No match group for interval. text=$text")

        return ScheduleResponse(day, interval)
    }

    data class ScheduleResponse(val day: Day, val interval: Interval)
}
