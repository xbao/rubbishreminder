package nz.xbc.rubbishreminder.rubbishtimes

import io.reactivex.Single
import nz.xbc.rubbishreminder.common.NetworkException
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url
import javax.inject.Inject

class RubbishTimesService @Inject constructor(private val restService: RubbishTimesRestService,
                                              private val rubbishTimesParser: RubbishTimesParser) {

    fun fetchAddressSchedule(addressKey: String): Single<RubbishTimesResponse> {
        return Single.create({ emitter ->
            try {
                val response = restService.getCollectionDay(key = addressKey).execute()
                val responseString = response?.body()?.string()
                if (responseString == null) {
                    emitter.onError(NetworkException("no response body"))
                } else {
                    emitter.onSuccess(rubbishTimesParser.parseResponse(responseString))
                }
            } catch (e: Exception) {
                emitter.onError(e)
            }
        })
    }

    interface RubbishTimesRestService {
        @GET
        fun getCollectionDay(@Url url: String = "https://www.aucklandcouncil.govt.nz/rubbish-recycling/rubbish-recycling-collections/Pages/collection-day-detail.aspx",
                             @Query("an") key: String)
                : Call<ResponseBody>
    }
}
