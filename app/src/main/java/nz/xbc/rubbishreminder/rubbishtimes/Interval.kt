package nz.xbc.rubbishreminder.rubbishtimes

/**
 * The collection interval. Not currently used (assumed weekly for rubbish, fortnightly for recycling)
 */
enum class Interval {
    WEEKLY,
    FORTNIGHTLY;

    companion object {
        private val intervalMap = hashMapOf(
                "weekly" to WEEKLY,
                "fortnightly" to FORTNIGHTLY
        )

        fun fromText(text: String): Interval? {
            return intervalMap[text.toLowerCase()]
        }
    }
}
