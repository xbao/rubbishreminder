package nz.xbc.rubbishreminder.rubbishtimes

import org.joda.time.DateTime

data class CollectionDateResponse(val date: DateTime, val hasRecycling: Boolean, val hasRubbish: Boolean)
