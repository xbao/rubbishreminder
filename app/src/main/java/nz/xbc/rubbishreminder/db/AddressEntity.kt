package nz.xbc.rubbishreminder.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address")
class AddressEntity {

    constructor(addressKey: String, address: String) {
        this.addressKey = addressKey
        this.address = address
    }

    constructor()

    @PrimaryKey
    var addressKey: String = ""
    var address: String = ""
}
