package nz.xbc.rubbishreminder.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AddressEntityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddress(address: AddressEntity)

    @Query("SELECT * FROM address WHERE addressKey = :addressKey")
    fun loadAddress(addressKey: String): AddressEntity?
}
