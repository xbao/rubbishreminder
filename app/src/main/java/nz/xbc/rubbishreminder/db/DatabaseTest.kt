package nz.xbc.rubbishreminder.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Database(entities = arrayOf(TestUserEntity::class,
        CurrentUserEntity::class), version = 1)
abstract class DatabaseTest : RoomDatabase() {
    abstract fun testUserDao(): TestUserDao
}

@Dao
interface TestUserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(testUser: TestUserEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCurrentUser(currentUser: CurrentUserEntity)

    @Query("SELECT * FROM user JOIN current_user USING (userId)")
    fun observeCurrentUser(): LiveData<TestUserEntity?>

    @Query("SELECT * FROM user JOIN current_user USING (userId)")
    fun getCurrentUser(): TestUserEntity?

}

@Entity(tableName = "user")
class TestUserEntity {
    @PrimaryKey
    var userId: String = ""
    var name: String = ""
}

@Entity(tableName = "current_user",
        foreignKeys = arrayOf(ForeignKey(entity = TestUserEntity::class,
                parentColumns = arrayOf("userId"),
                childColumns = arrayOf("userId"))))
class CurrentUserEntity {
    @PrimaryKey
    var userId: String = ""
}
