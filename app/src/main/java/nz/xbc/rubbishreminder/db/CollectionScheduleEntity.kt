package nz.xbc.rubbishreminder.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "collection_schedule",
        foreignKeys = arrayOf(ForeignKey(entity = AddressEntity::class,
                parentColumns = arrayOf("addressKey"),
                childColumns = arrayOf("addressKey"))))
data class CollectionScheduleEntity(
        @PrimaryKey(autoGenerate = true)
        var scheduleId: Long? = null,
        var addressKey: String? = null,
        var day: Int = 0,
        var hasRubbish: Boolean = false,
        var hasRecycling: Boolean = false,
        var recyclingDate: Long = -1)
