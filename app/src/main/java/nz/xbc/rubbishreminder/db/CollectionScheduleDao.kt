package nz.xbc.rubbishreminder.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable

@Dao
abstract class CollectionScheduleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSchedules(vararg collectionScheduleEntities: CollectionScheduleEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSchedule(collectionScheduleEntity: CollectionScheduleEntity): Long

    @Query("SELECT * FROM collection_schedule WHERE scheduleId = :scheduleId")
    abstract fun loadSchedule(scheduleId: Long): CollectionScheduleEntity?

    @Query("SELECT * FROM collection_schedule WHERE addressKey = :addressKey")
    abstract fun loadScheduleWithAddress(addressKey: String): CollectionScheduleEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCurrentSchedule(currentSchedule: CurrentScheduleEntity)

    @Query("SELECT * FROM collection_schedule JOIN current_schedule USING (scheduleId)")
    abstract fun observeCurrentSchedule(): Flowable<CollectionScheduleEntity>

    @Query("SELECT * FROM collection_schedule JOIN current_schedule USING (scheduleId)")
    abstract fun loadCurrentSchedule(): CollectionScheduleEntity?

    @Query("DELETE FROM current_schedule")
    abstract fun clearCurrentSchedule()

    @Query("UPDATE collection_schedule SET addressKey = :addressKey WHERE scheduleId = :scheduleId")
    abstract fun setScheduleAddress(addressKey: String, scheduleId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCollectionDates(collectionDate: List<CollectionDateEntity>)

    @Query("SELECT * FROM collection_dates WHERE scheduleId = :scheduleId")
    abstract fun loadCollectionDates(scheduleId: Long): List<CollectionDateEntity?>

    @Query("SELECT * FROM collection_dates JOIN current_schedule USING (scheduleId) ORDER BY date ASC")
    abstract fun observeCurrentCollectionDates(): Flowable<List<CollectionDateEntity>>

    @Query("SELECT * FROM collection_dates JOIN current_schedule USING (scheduleId) ORDER BY date ASC")
    abstract fun getCollectionDates(): List<CollectionDateEntity?>

    @Query("DELETE FROM collection_dates WHERE date < :time")
    abstract fun clearCollectionTimesBefore(time: Long)
}
