package nz.xbc.rubbishreminder.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context


@Database(entities = arrayOf(AddressEntity::class,
        CollectionScheduleEntity::class,
        CollectionDateEntity::class,
        CurrentScheduleEntity::class), version = 4)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        fun create(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, "rubbish-db")
                .fallbackToDestructiveMigration()
                .build()

    }

    abstract fun schedulesDao(): CollectionScheduleDao
    abstract fun addressDao(): AddressEntityDao
}
