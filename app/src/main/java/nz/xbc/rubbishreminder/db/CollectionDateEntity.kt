package nz.xbc.rubbishreminder.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "collection_dates",
        primaryKeys = arrayOf("weekId", "scheduleId"))
class CollectionDateEntity {

    constructor(weekId: Int, scheduleId: Long, date: Long, includesRubbish: Boolean, includesRecycling: Boolean,
                isConfirmed: Boolean) {
        this.weekId = weekId
        this.scheduleId = scheduleId
        this.date = date
        this.includesRubbish = includesRubbish
        this.includesRecycling = includesRecycling
        this.isConfirmed = isConfirmed
    }

    constructor()

    var weekId: Int = 0
    @ForeignKey(entity = CollectionScheduleEntity::class,
            parentColumns = arrayOf("scheduleId"),
            childColumns = arrayOf("scheduleId"))
    var scheduleId: Long = 0
    var date: Long = 0
    var includesRubbish: Boolean = false
    var includesRecycling: Boolean = false
    var isConfirmed: Boolean = false
}
