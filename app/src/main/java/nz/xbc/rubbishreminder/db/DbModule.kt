package nz.xbc.rubbishreminder.db

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(private val appDatabase: AppDatabase) {

    @Provides
    @Singleton
    fun provideCollectionScheduleDao(): CollectionScheduleDao {
        return appDatabase.schedulesDao()
    }

    @Provides
    @Singleton
    fun provideAddressDao(): AddressEntityDao {
        return appDatabase.addressDao()
    }
}
