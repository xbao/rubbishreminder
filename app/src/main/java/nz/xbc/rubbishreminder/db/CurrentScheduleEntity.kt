package nz.xbc.rubbishreminder.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "current_schedule")
class CurrentScheduleEntity {

    constructor(scheduleId: Long) {
        this.scheduleId = scheduleId
    }

    constructor()

    @PrimaryKey
    var dummyPrimaryKey = 1

    var scheduleId: Long = 0
    override fun toString(): String {
        return "CurrentScheduleEntity(dummyPrimaryKey=$dummyPrimaryKey, scheduleId=$scheduleId)"
    }
}
