package nz.xbc.rubbishreminder.main

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import nz.xbc.rubbishreminder.R

class CollectionScheduleItemViewHolder(val view: View) {
    val dayTextView = view.findViewById<TextView>(R.id.day_text)!!
    val intervalTextView = view.findViewById<TextView>(R.id.interval_text)!!
    val iconImageView = view.findViewById<ImageView>(R.id.icon_image)!!
    fun bind(data: CollectionScheduleItemData) {
        dayTextView.text = data.dayText
        intervalTextView.text = data.intervalText
        iconImageView.setImageResource(data.iconResource)
    }
}