package nz.xbc.rubbishreminder.main

data class CollectionScheduleViewData(val rubbishSchedule: CollectionScheduleItemData?, val recyclingSchedule: CollectionScheduleItemData?)