package nz.xbc.rubbishreminder.main

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import nz.xbc.rubbishreminder.R
import nz.xbc.multiadapter.MultiAdapter
import nz.xbc.rubbishreminder.util.inflate

class AddressSubadapter(val clickListener: () -> Unit) : MultiAdapter.Subadapter<AddressViewHolder>() {

    var addressText: String = ""
        set(value) {
            field = value
            notifyItemChanged(0)
        }

    override fun getItemCount(): Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        return AddressViewHolder(parent.inflate(R.layout.main_address_item))
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.addressTextView.text = addressText
        holder.setAddressView.setOnClickListener { clickListener.invoke() }
    }

    override fun onUnbindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.setAddressView.setOnClickListener(null)
    }

}

class AddressViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val addressTextView = view.findViewById<TextView>(R.id.textView)!!
    val setAddressView = view.findViewById<View>(R.id.set_address_view)!!
}
