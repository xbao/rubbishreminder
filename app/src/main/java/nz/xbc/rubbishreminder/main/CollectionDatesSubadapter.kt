package nz.xbc.rubbishreminder.main

import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.collection.CollectionDate
import nz.xbc.multiadapter.MultiAdapter
import nz.xbc.rubbishreminder.util.inflate
import nz.xbc.rubbishreminder.util.visibleIf

class CollectionDatesSubadapter : MultiAdapter.Subadapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    companion object {
        val HEADER_TYPE = 0
        val ITEM_TYPE = 1
    }

    var collectionDates: List<CollectionDate> = emptyList()
        set(value) {
            val old = field
            field = value
            if (old.isEmpty() && value.isNotEmpty()) {
                notifyItemRangeInserted(0, value.size + 1)
            } else if (old.isNotEmpty() && value.isEmpty()) {
                notifyItemRangeRemoved(0, old.size + 1)
            } else {
                // give up - could be smarter about this if really required
                notifyDataSetChanged()
            }
        }

    override fun getItemCount(): Int = if (collectionDates.isEmpty()) 0 else collectionDates.size + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        when (viewType) {
            HEADER_TYPE -> {
                return object : androidx.recyclerview.widget.RecyclerView.ViewHolder(parent.inflate(R.layout.main_collection_header)) {}
            }
            ITEM_TYPE -> {
                return CollectionDatesViewHolder(parent.inflate(R.layout.main_collection_dates_item))
            }
        }
        throw IllegalArgumentException("Unknown viewtype: $viewType")
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CollectionDatesViewHolder -> {
                val dates = collectionDates[position - 1]
                holder.dateTextView.text = createDateText(dates)
                holder.rubbishImageView.visibleIf(dates.includeRubbish)
                holder.recycleImageView.visibleIf(dates.includeRecycling)
            }
        }
    }

    private fun createDateText(date: CollectionDate): CharSequence {
        val dateText = date.date.toString("EEEE dd MMMM")
        if (date.isConfirmed)
            return dateText

        val spannable = SpannableStringBuilder(dateText)

        val appendText = " (scheduled)"
        spannable.append(appendText)
        spannable.setSpan(StyleSpan(Typeface.ITALIC), spannable.length - appendText.length, spannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannable
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) HEADER_TYPE else ITEM_TYPE
    }
}

class CollectionDatesViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val dateTextView = view.findViewById<TextView>(R.id.date_text)!!
    val rubbishImageView = view.findViewById<ImageView>(R.id.rubbish_image)!!
    val recycleImageView = view.findViewById<ImageView>(R.id.recycle_image)!!
}
