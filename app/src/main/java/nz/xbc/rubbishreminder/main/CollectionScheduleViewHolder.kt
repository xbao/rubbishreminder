package nz.xbc.rubbishreminder.main

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import nz.xbc.rubbishreminder.R

class CollectionScheduleViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val rubbishItem = CollectionScheduleItemViewHolder(view.findViewById<View>(R.id.rubbish_item_layout))
    val recyclingItem = CollectionScheduleItemViewHolder(view.findViewById(R.id.recycling_item_layout))
}