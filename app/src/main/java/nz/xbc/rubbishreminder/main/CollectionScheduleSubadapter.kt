package nz.xbc.rubbishreminder.main

import android.view.ViewGroup
import nz.xbc.rubbishreminder.R
import nz.xbc.multiadapter.MultiAdapter
import nz.xbc.rubbishreminder.util.inflate
import nz.xbc.rubbishreminder.util.visibleIf

class CollectionScheduleSubadapter : MultiAdapter.Subadapter<CollectionScheduleViewHolder>() {
    var data: CollectionScheduleViewData? = null
        set (value) {
            val old = field
            field = value
            if (old == null && value != null) {
                notifyItemInserted(0)
            } else if (old != null && value == null){
                notifyItemRemoved(0)
            } else {
                notifyItemChanged(0)
            }
        }

    override fun getItemCount(): Int = if (data == null) 0 else 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionScheduleViewHolder {
        return CollectionScheduleViewHolder(parent.inflate(R.layout.main_collection_schedule_card))
    }

    override fun onBindViewHolder(holder: CollectionScheduleViewHolder, position: Int) {
        val currData = data ?: return
        if (currData.rubbishSchedule != null) {
            holder.rubbishItem.bind(currData.rubbishSchedule)
        }
        if (currData.recyclingSchedule != null) {
            holder.recyclingItem.bind(currData.recyclingSchedule)
        }
        holder.rubbishItem.view.visibleIf(currData.rubbishSchedule != null)
        holder.recyclingItem.view.visibleIf(currData.recyclingSchedule != null)
    }

}
