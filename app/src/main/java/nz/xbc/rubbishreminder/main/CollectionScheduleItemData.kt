package nz.xbc.rubbishreminder.main

data class CollectionScheduleItemData(val dayText: String, val intervalText: String, val iconResource: Int)