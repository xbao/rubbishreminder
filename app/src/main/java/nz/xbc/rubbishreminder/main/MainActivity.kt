package nz.xbc.rubbishreminder.main

import android.app.Activity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.widget.ProgressBar
import io.reactivex.android.schedulers.AndroidSchedulers
import nz.xbc.multiadapter.MultiAdapter
import nz.xbc.multiadapter.SimpleSubadapter
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.about.AboutActivity
import nz.xbc.rubbishreminder.addressselection.ChooseAddressActivity
import nz.xbc.rubbishreminder.addschedule.AddScheduleActivity
import nz.xbc.rubbishreminder.common.BaseActivity
import nz.xbc.rubbishreminder.common.FeatureToggle.MANUAL_SCHEDULES
import nz.xbc.rubbishreminder.util.genericErrorDialog
import nz.xbc.rubbishreminder.util.visibleIf
import kotlin.properties.Delegates

class MainActivity : BaseActivity() {

    companion object {
        val TAG = "MainActivity"
        val ADD_SCHEDULE_RQ = 1
        fun createIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
    }

    lateinit var viewModel: MainViewModel

    private val addressSubadapter = AddressSubadapter(clickListener = {
        if (MANUAL_SCHEDULES) {
            startActivity(Intent(this, AddScheduleActivity::class.java));
        } else {
            startActivity(Intent(this, ChooseAddressActivity::class.java))
        }
    })

    private val collectionDatesSubadapter = CollectionDatesSubadapter()
    private val collectionScheduleSubadapter = CollectionScheduleSubadapter()

    private var progressBar: ProgressBar by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)

        viewModel =
                ViewModelProviders.of(this@MainActivity, object : ViewModelProvider.Factory {
                    @Suppress("UNCHECKED_CAST")
                    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                        return appComponent.mainViewModel() as T
                    }
                }).get(MainViewModel::class.java)

        // Needs to be synchronous so that the user doesn't see two activities pop up
        if (viewModel.navigateToAddScheduleOnStart()) {
            if (MANUAL_SCHEDULES) {
                startActivity(Intent(this, AddScheduleActivity::class.java));
            } else {
                startActivity(Intent(this, ChooseAddressActivity::class.java))
            }
        }


        val recycler = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recyclerView)
        val adapter = MultiAdapter(addressSubadapter,
            collectionScheduleSubadapter,
            collectionDatesSubadapter,
            SimpleSubadapter(R.layout.main_about_text) {
                startActivity(Intent(this@MainActivity, AboutActivity::class.java))
            })
        recycler.adapter = adapter

    }

    override fun onStart() {
        super.onStart()
        viewModel.viewState
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ viewState ->
                    viewState.address?.let {
                        addressSubadapter.addressText = it
                    }

                    viewState.collectionDates.error?.let {
                        genericErrorDialog(it)
                    }
                    viewState.collectionDates.data?.let {
                        collectionDatesSubadapter.collectionDates = it
                    }

                    viewState.scheduleViewData?.let {
                        collectionScheduleSubadapter.data = it
                    }
                    progressBar.visibleIf(viewState.collectionDates.isLoading)
                })
                .let { onPauseDisposables.add(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ADD_SCHEDULE_RQ -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.onScheduleAdded();
                } else {
                    viewModel.onScheduleError();
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
