package nz.xbc.rubbishreminder.main

import androidx.lifecycle.ViewModel
import android.content.res.Resources
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.addressselection.AddressRepo
import nz.xbc.rubbishreminder.collection.CollectionDate
import nz.xbc.rubbishreminder.collection.CollectionRepo
import nz.xbc.rubbishreminder.collection.CollectionSchedule
import nz.xbc.rubbishreminder.common.Result
import nz.xbc.rubbishreminder.util.mutate
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(val addressRepo : AddressRepo,
                                        val collectionRepo : CollectionRepo,
                                        val resources: Resources) : ViewModel() {

    val viewState = BehaviorSubject.createDefault<MainViewState>(MainViewState(null, Result(), null))

    private val disposables: CompositeDisposable = CompositeDisposable()

    init {
        collectionRepo.observeSchedule()
                .subscribe { schedule ->
                    schedule!!
                    Timber.d("Schedule updated: $schedule");

                    val addressText = if (schedule.addressKey != null) {
                        refreshRubbishTimes(schedule.addressKey)
                        addressRepo.getAddress(schedule.addressKey)?.address
                    } else {
                        resources.getString(R.string.no_address_set)
                    }
                    viewState.mutate {
                        it.copy(scheduleViewData = schedule.mapToViewData(),
                                address = addressText)
                    }
                }
                .let { disposables.add(it) }

        collectionRepo.observeCollectionDates()
                .subscribe { dates ->
                    viewState.mutate { it.copy(collectionDates = it.collectionDates.copy(data = dates)) }
                }
                .let { disposables.add(it) }

        collectionRepo.removeOutdatedCollectionDates().subscribe()
                .let { disposables.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun navigateToAddScheduleOnStart(): Boolean {
        return !collectionRepo.hasScheduleAvailable();
    }

    private fun refreshRubbishTimes(addressKey: String) {
        if (viewState.value.collectionDates.isLoading) {
            return
        }
        viewState.mutate {
            it.copy(collectionDates = it.collectionDates.copy(isLoading = true))
        }

        collectionRepo.fetchCollectionSchedule(addressKey)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.mutate {
                        it.copy(collectionDates = it.collectionDates.copy(error = null,
                                isLoading = false))
                    }
                }, { error ->
                    Timber.e(error, "Error getting rubbish times")
                    viewState.mutate { viewState ->
                        viewState.copy(collectionDates = viewState.collectionDates.copy(
                                error = error, isLoading = false))
                    }
                })
    }

    private fun CollectionSchedule?.mapToViewData(): CollectionScheduleViewData? {
        if (this == null) {
            return null
        }
        return CollectionScheduleViewData(
                rubbishSchedule = if (!hasRubbish) null else {
                    CollectionScheduleItemData(dayText = day.name.capitalize(),
                            intervalText = "weekly",
                            iconResource = R.drawable.ic_rubbish)
                },
                recyclingSchedule = if (!hasRecycling) null else {
                    CollectionScheduleItemData(dayText = day.name.capitalize(),
                            intervalText = "fortnightly",
                            iconResource = R.drawable.ic_recycle)
                })
    }

    fun onScheduleAdded() {

    }

    fun onScheduleError() {
    }


}

data class MainViewState(val scheduleViewData: CollectionScheduleViewData?,
                         val collectionDates: Result<List<CollectionDate>>,
                         val address: String?)
