package nz.xbc.rubbishreminder.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import nz.xbc.rubbishreminder.App
import timber.log.Timber

class ReminderBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != Intent.ACTION_BOOT_COMPLETED) {
            return
        }
        Timber.i("ACTION_BOOT_COMPLETED received. Calling setNextAlarm")
        (context.applicationContext as App).appComponent.reminderManager().setNextAlarm()
    }

}