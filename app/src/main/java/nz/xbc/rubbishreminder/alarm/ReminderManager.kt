package nz.xbc.rubbishreminder.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import io.reactivex.disposables.Disposable
import nz.xbc.rubbishreminder.collection.CollectionDate
import nz.xbc.rubbishreminder.collection.CollectionRepo
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReminderManager @Inject constructor(val collectionRepo: CollectionRepo, val alarmManager: AlarmManagerFacade) {
    // Set to true to have an alarm go 15 seconds after the current time
    private val debugAlarm = false

    private var disposable: Disposable? = null
        set(value) {
            field?.dispose()
            field = value
        }

    fun startObservingDates() {
        disposable = collectionRepo.observeCollectionDates()
                .subscribe({ datesRaw ->
                    findAndSetNextAlarm(datesRaw ?: emptyList())
                })
    }

    fun stopObservingDates() {
        disposable?.dispose()
    }

    fun setNextAlarm() {
        findAndSetNextAlarm(collectionRepo.getCollectionDates())
    }

    private fun findAndSetNextAlarm(dates: List<CollectionDate>) {
        Timber.d("findAndSetNextAlarm dates=$dates")
        dates.find {
            reminderDate(it.date).isAfterNow
        }?.let { setAlarm(it) }
                ?: Timber.d("No alarm required")
    }

    private fun setAlarm(date: CollectionDate) {
        Timber.d("Setting alarm for $date")
        val alarmDate = reminderDate(date.date)
        if (alarmDate.isBeforeNow) {
            Timber.d("Not setting alarm - date is before now")
            return
        }

        if (debugAlarm) {
            Timber.d("Setting debug alarm")
            alarmManager.setAlarm(
                    DateTime.now().plusSeconds(15).millis,
                    date)
            return
        }
        alarmManager.setAlarm(
                alarmDate.millis,
                date)
    }

    private fun reminderDate(date: DateTime): DateTime {
        return date.minusDays(1).withHourOfDay(18)!!
    }
}

class AlarmManagerFacade @Inject constructor(val context: Context) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    init {
        val pendingIntent = PendingIntent.getBroadcast(context,
                0,
                NotificationReceiver.createEmptyIntent(context),
                PendingIntent.FLAG_NO_CREATE)
        if (pendingIntent == null) {
            Timber.d("No alarm currently scheduled")
        } else {
            Timber.d("Alarm is scheduled")
        }
    }

    fun setAlarm(alarmTime: Long, date: CollectionDate) {
        Timber.d("Alarm for $date set to run in ${(alarmTime - DateTime.now().millis) / (1000 * 60 * 60)} hours")
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                alarmTime,
                createPendingIntent(date))
    }

    fun cancelAlarm() {
        alarmManager.cancel(createPendingIntent())
    }

    fun createPendingIntent(date: CollectionDate? = null): PendingIntent {
        val intent = if (date == null) {
            NotificationReceiver.createEmptyIntent(context)
        } else {
            NotificationReceiver.createIntent(context, date)
        }
        Timber.d("getting pending intent with keyset: ${intent.extras.keySet().toList()}")
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}
