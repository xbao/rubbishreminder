package nz.xbc.rubbishreminder.alarm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import io.reactivex.schedulers.Schedulers
import nz.xbc.rubbishreminder.R
import nz.xbc.rubbishreminder.collection.CollectionDate
import nz.xbc.rubbishreminder.common.di.appComponent
import nz.xbc.rubbishreminder.main.MainActivity
import timber.log.Timber
import javax.inject.Inject

class NotificationReceiver : BroadcastReceiver() {

    @Inject
    lateinit var reminderManager: ReminderManager

    companion object {
        const val KEY_INCLUDE_RECYCLING = "nz.xbc.rubbishreminder.KEY_INCLUDE_RECYCLING"
        const val KEY_INCLUDE_RUBBISH = "nz.xbc.rubbishreminder.KEY_INCLUDE_RUBBISH"

        const val NOTIFICATION_CHANNEL_ID = "rubbish_reminder_channel_id"

        fun createEmptyIntent(context: Context) =
                Intent(context, NotificationReceiver::class.java)

        fun createIntent(context: Context, date: CollectionDate): Intent {
            return createEmptyIntent(context)
                    .putExtra(KEY_INCLUDE_RECYCLING, date.includeRecycling)
                    .putExtra(KEY_INCLUDE_RUBBISH, date.includeRubbish)
        }

    }

    override fun onReceive(context: Context, intent: Intent?) {
        Timber.d("NotificationReceiver onReceive")

        if (!::reminderManager.isInitialized) {
            context.appComponent().inject(this)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Timber.d("NotificationReceiver creating notification channel")
            createNotificationChannel(context)
        }

        if (intent != null) {
            Timber.d("NotificationReceiver got intent with keys: ${intent.extras.keySet().toList()}")
            postNotification(context = context,
                    includeRubbish = intent.getBooleanExtra(KEY_INCLUDE_RUBBISH, false),
                    includeRecycling = intent.getBooleanExtra(KEY_INCLUDE_RECYCLING, false))
            Schedulers.io().scheduleDirect {
                reminderManager.setNextAlarm()
            }
        } else {
            Timber.e("null intent in NotificationReceiver")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context) {
        context.notificationManager.createNotificationChannel(NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "Rubbish/Recycling reminders",
                NotificationManager.IMPORTANCE_DEFAULT
        ))
    }

    private fun postNotification(context: Context, includeRubbish: Boolean, includeRecycling: Boolean) {
        if (!includeRubbish && !includeRecycling) {
            Timber.e("no rubbish or recycling to be notified")
            return
        }

        val content = StringBuilder("Take out your ")
        if (includeRubbish) {
            content.append("rubbish")
        }
        if (includeRecycling) {
            if (includeRubbish) {
                content.append(" and ")
            }
            content.append("recycling")
        }
        Timber.i("Posting notification")
        val notification = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setContentTitle("Rubbish Reminder")
                .setContentText(content.toString())
                .setSmallIcon(if (includeRecycling) R.drawable.ic_recycle else R.drawable.ic_rubbish)
                .setContentIntent(PendingIntent.getActivity(context, 0, MainActivity.createIntent(context), 0))
                .build()
        context.notificationManager.notify(0, notification)
    }


}

val Context.notificationManager: NotificationManager get() = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager