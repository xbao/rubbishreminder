package nz.xbc.multiadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import nz.xbc.rubbishreminder.BuildConfig
import java.util.ArrayList
import kotlin.properties.Delegates

class MultiAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {

    constructor() : super()
    constructor(vararg subadapters: Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>) : super() {
        addSubadapters(subadapters.asList())
    }

    private val helper = MultiAdapterHelper()

    private val subAdapterListener = object :
        SubAdapterListener<Any> {

        override fun notifyDataSetChanged(subadapter: Subadapter<out Any>) {
            notifyDataSetChanged()
        }

        override fun notifyItemRangeRemoved(subadapter: Subadapter<out Any>, position: Int, itemCount: Int) {
            notifyItemRangeRemoved(helper.getSubadapterItemBasePosition(subadapter) + position, itemCount)
        }

        override fun notifyItemRemoved(subadapter: Subadapter<out Any>, position: Int) {
            notifyItemRangeRemoved(subadapter, position, 1)
        }

        override fun notifyItemRangeInserted(subadapter: Subadapter<out Any>, position: Int, itemCount: Int) {

            notifyItemRangeInserted(helper.getSubadapterItemBasePosition(subadapter) + position, itemCount)
        }

        override fun notifyItemInserted(subadapter: Subadapter<out Any>, position: Int) {
            notifyItemRangeInserted(subadapter, position, 1)
        }

        override fun notifyItemRangeChanged(subadapter: Subadapter<out Any>, position: Int, itemCount: Int) {
            notifyItemRangeChanged(helper.getSubadapterItemBasePosition(subadapter) + position, itemCount)
        }

        override fun notifyItemChanged(subadapter: Subadapter<out Any>, position: Int) {
            notifyItemRangeChanged(subadapter, position, 1)
        }
    }

    fun addSubadapters(subadapters: List<Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>>) {
        for (adapter in subadapters) {
            addSubadapter(adapter)
        }
    }

    fun addSubadapter(subadapter: Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>) {
        subadapter.listener = subAdapterListener
        helper.add(subadapter)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return helper.getSubadapterFromViewType(viewType)
                .onCreateViewHolder(parent, helper.removeIndexFromViewType(viewType))
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        helper.getSubadapterFromItemPosition(position)
                .onBindViewHolderInternal(holder, helper.adjustItemPosition(position))
    }

    override fun onViewDetachedFromWindow(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        val position = holder.adapterPosition
        if (position != androidx.recyclerview.widget.RecyclerView.NO_POSITION) {
            helper.getSubadapterFromItemPosition(position)
                    .onUnbindViewHolderInternal(holder, helper.adjustItemPosition(position))
        }
    }

    override fun getItemCount(): Int {
        return helper.getItemCount()
    }

    override fun getItemViewType(position: Int): Int {
        return helper.getItemViewType(position)
    }

    class MultiAdapterHelper {
        private val subadapters = ArrayList<Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>>()

        fun getSubadapter(subAdapterIndex: Int): Subadapter<*> {
            return subadapters[subAdapterIndex]
        }

        fun getItemCount(): Int {
            return subadapters.sumBy { it.getItemCount() }
        }

        fun removeIndexFromViewType(viewType: Int): Int {
            return viewType and VIEW_TYPE_INDEX_MASK.inv()
        }

        fun getIndexFromViewType(viewType: Int): Int {
            return viewType.ushr(24)
        }

        fun addIndexToViewType(adapterIndex: Int, viewType: Int): Int {
            return (viewType and VIEW_TYPE_INDEX_MASK.inv()) or adapterIndex.shl(24)
        }

        fun getItemViewType(position: Int): Int {
            val subadapterIndex = getSubadapterIndexFromPosition(position)
            val subadapter = subadapters[subadapterIndex]
            val basePosition = getSubadapterItemBasePosition(subadapter)
            return addIndexToViewType(subadapterIndex, subadapter.getItemViewType(position - basePosition))
        }

        fun adjustItemPosition(position: Int): Int {
            return position - getSubadapterItemBasePosition(getSubadapterFromItemPosition(position))
        }

        fun getSubadapterItemBasePosition(subadapter: Subadapter<*>): Int {
            return getSubadapterItemBasePosition(subadapters.indexOf(subadapter))
        }

        private fun getSubadapterItemBasePosition(adapterIndex: Int): Int {
            var count = 0
            for (i in subadapters.indices) {
                if (i == adapterIndex) {
                    return count
                }
                count += subadapters[i].getItemCount()
            }
            throw IllegalArgumentException("Invalid adapter index: $adapterIndex. adapter count=${subadapters.size}")
        }

        fun add(subadapter: Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>) {
            subadapters.add(subadapter)
        }

        fun getSubadapterFromViewType(viewType: Int): Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder> {
            val index = getSubadapterIndexFromViewType(viewType)
            if (BuildConfig.DEBUG) {
                if (index !in subadapters.indices) {
                    throw IllegalArgumentException(String.format("Attempt to get index %x from array of size %d. viewType=%x", index, subadapters.size, viewType))
                }
            }
            return subadapters[index]
        }

        fun getSubadapterFromItemPosition(position: Int): Subadapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder> {
            return subadapters[getSubadapterIndexFromPosition(position)]
        }

        private fun getSubadapterIndexFromViewType(viewType: Int): Int {
            return (viewType and VIEW_TYPE_INDEX_MASK).ushr(24)
        }

        private fun getSubadapterIndexFromPosition(position: Int): Int {
            var count = 0
            for (i in subadapters.indices) {
                val subadapter = subadapters[i]
                count += subadapter.getItemCount()
                if (position < count) {
                    return i
                }
            }

            throw IllegalArgumentException("Unable to find subadapter for position " + position)
        }
    }

    abstract class Subadapter<SubVH> {

        var listener by Delegates.notNull<SubAdapterListener<SubVH>>()

        abstract fun getItemCount(): Int

        abstract fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubVH

        fun onBindViewHolderInternal(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
            @Suppress("UNCHECKED_CAST")
            onBindViewHolder(holder as SubVH, position)
        }

        abstract fun onBindViewHolder(holder: SubVH, position: Int)

        fun onUnbindViewHolderInternal(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
            @Suppress("UNCHECKED_CAST")
            onUnbindViewHolder(holder as SubVH, position)
        }

        open fun onUnbindViewHolder(holder: SubVH, position: Int) {}

        open fun getItemViewType(position: Int): Int {
            return 0
        }

        fun notifyDataSetChanged() {
            listener.notifyDataSetChanged(this)
        }

        fun notifyItemChanged(position: Int) {
            listener.notifyItemChanged(this, position)
        }

        fun notifyItemRangeChanged(position: Int, itemCount: Int) {
            listener.notifyItemRangeChanged(this, position, itemCount)
        }

        fun notifyItemRemoved(position: Int) {
            listener.notifyItemRemoved(this, position)
        }

        fun notifyItemRangeRemoved(position: Int, itemCount: Int) {
            listener.notifyItemRangeRemoved(this, position, itemCount)
        }

        fun notifyItemInserted(position: Int) {
            listener.notifyItemInserted(this, position)
        }

        fun notifyItemRangeInserted(position: Int, itemCount: Int) {
            listener.notifyItemRangeInserted(this, position, itemCount)
        }


    }

    interface SubAdapterListener<in VH> {
        fun notifyDataSetChanged(subadapter: Subadapter<out VH>)
        fun notifyItemChanged(subadapter: Subadapter<out VH>, position: Int)
        fun notifyItemRangeChanged(subadapter: Subadapter<out VH>, position: Int, itemCount: Int)
        fun notifyItemRemoved(subadapter: Subadapter<out VH>, position: Int)
        fun notifyItemRangeRemoved(subadapter: Subadapter<out VH>, position: Int, itemCount: Int)
        fun notifyItemInserted(subadapter: Subadapter<out VH>, position: Int)
        fun notifyItemRangeInserted(subadapter: Subadapter<out VH>, position: Int, itemCount: Int)
    }

    companion object {
        val VIEW_TYPE_INDEX_MASK: Int = -0x01_00_00_00 // 0xFF_00_00_00
    }
}
