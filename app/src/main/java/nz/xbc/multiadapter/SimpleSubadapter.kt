package nz.xbc.multiadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import nz.xbc.rubbishreminder.util.inflate

class SimpleViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

class SimpleSubadapter(
    var layoutId: Int,
    var onClick: ((View) -> Unit)? = null
) : MultiAdapter.Subadapter<SimpleViewHolder>() {


    override fun getItemCount(): Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(parent.inflate(layoutId).also {
            it.setOnClickListener {
                onClick?.invoke(it)
            }
        })
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {

    }

}
