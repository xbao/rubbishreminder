package nz.xbc.rubbishreminder.rubbishtimes

import nz.xbc.rubbishreminder.common.Clock
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.test.DateTestUtils.date
import org.joda.time.DateTime
import org.junit.Assert.*
import org.junit.Test

class RubbishTimesParserTest {
    companion object {
        fun readResource(name: String) = RubbishTimesParserTest::class.java.classLoader
                .getResourceAsStream(name)
                .bufferedReader()
                .use { it.readText() }
    }

    val sampleHtml: String = readResource("sample_response.html")
    val sampleHtmlXmas: String = readResource("sample_response_xmas.html")
    val sampleHtmlNoRecycling: String = readResource("sample_response_no_recycling.html")

    fun parser(mockedCurrentDate: DateTime = date("Tue 04 July 2017")): RubbishTimesParser {
        return RubbishTimesParser(Clock { mockedCurrentDate })
    }

    @Test
    fun parseResponse() {
        val rubbishTime = parser().parseResponse(sampleHtml)
        assertEquals(Day.WEDNESDAY, rubbishTime.rubbishCollectionDay)
        assertEquals(Interval.WEEKLY, rubbishTime.rubbishCollectionInterval)
        assertEquals(Day.WEDNESDAY, rubbishTime.recyclingCollectionDay)
        assertEquals(Interval.FORTNIGHTLY, rubbishTime.recyclingCollectionInterval)

        assertEquals(2, rubbishTime.dates.size)
        assertEquals(1, rubbishTime.dates[0].date.dayOfMonth().get())
        assertEquals(11, rubbishTime.dates[0].date.monthOfYear().get())
        assertEquals(true, rubbishTime.dates[0].hasRecycling)
        assertEquals(true, rubbishTime.dates[0].hasRubbish)
        assertEquals(false, rubbishTime.dates[1].hasRecycling)
        assertEquals(true, rubbishTime.dates[1].hasRubbish)

    }

    @Test
    fun parseResponse_xmas() {
        val rubbishTime = parser(date("Tue 26 Dec 2017")).parseResponse(sampleHtmlXmas)
        assertEquals(Day.TUESDAY, rubbishTime.rubbishCollectionDay)
        assertEquals(Interval.WEEKLY, rubbishTime.rubbishCollectionInterval)
        assertEquals(Day.TUESDAY, rubbishTime.recyclingCollectionDay)
        assertEquals(Interval.FORTNIGHTLY, rubbishTime.recyclingCollectionInterval)

        assertEquals(2, rubbishTime.dates.size)
        rubbishTime.dates[0].let {
            assertFalse(it.hasRecycling)
            assertTrue(it.hasRubbish)
            assertEquals(date("Wed 27 Dec 2017"), it.date)

        }
        rubbishTime.dates[1].let {
            assertTrue(it.hasRecycling)
            assertTrue(it.hasRubbish)
            assertEquals(date("Wed 03 Jan 2018"), it.date)
        }
    }

    @Test
    fun parseCollectionScheduleText_oneDay_weekly() {
        testParseCollectionScheduleText(text = "Collection day: Thursday, weekly except after a public holiday.",
                expectedDay = Day.THURSDAY,
                expectedInterval = Interval.WEEKLY)
        testParseCollectionScheduleText(text = "Collection day: Monday, weekly except after a public holiday.",
                expectedDay = Day.MONDAY,
                expectedInterval = Interval.WEEKLY)
        testParseCollectionScheduleText(text = "There is no council rubbish collection service available in this area.",
                expectedDay = null,
                expectedInterval = null)
    }

    @Test
    fun parseResponse_noRecycling() {
        val response = parser().parseResponse(sampleHtmlNoRecycling)

        assertNull(response.rubbishCollectionDay)
        assertNull(response.rubbishCollectionInterval)

        assertEquals(Day.WEDNESDAY, response.recyclingCollectionDay)
        assertEquals(Interval.FORTNIGHTLY, response.recyclingCollectionInterval)
        assertEquals(1, response.dates.size)

        response.dates[0].let { date ->

            assertEquals(true, date.hasRecycling)
            assertEquals(false, date.hasRubbish)
            assertEquals("Wednesday", date.date.dayOfWeek().asText)
            assertEquals(8, date.date.dayOfMonth().get())
            assertEquals("November", date.date.monthOfYear().asText)
        }
    }

    private fun testParseCollectionScheduleText(text: String, expectedDay: Day?, expectedInterval: Interval?) {
        val schedule = parser().parseCollectionScheduleText(text)
        assertEquals(expectedDay, schedule?.day)
        assertEquals(expectedInterval, schedule?.interval)

    }
}
