package nz.xbc.rubbishreminder.test.factory

import nz.xbc.rubbishreminder.common.WeekIds
import nz.xbc.rubbishreminder.collection.CollectionDate
import org.joda.time.DateTime

object CollectionDateFactory {

    fun createCollectionDate(
            date: DateTime,
            includeRubbish: Boolean = true,
            includeRecycling: Boolean = false,
            isConfirmed: Boolean = false): CollectionDate {
        return createCollectionDate(WeekIds.convertToWeekId(date), date, includeRubbish, includeRecycling, isConfirmed)
    }

    fun createCollectionDate(
            weekId: Int,
            includeRubbish: Boolean = true,
            includeRecycling: Boolean = false,
            isConfirmed: Boolean = false): CollectionDate {
        return createCollectionDate(weekId, WeekIds.convertToDateTime(weekId), includeRubbish, includeRecycling, isConfirmed)
    }

    fun createCollectionDate(
            weekId: Int,
            date: DateTime,
            includeRubbish: Boolean = true,
            includeRecycling: Boolean = false,
            isConfirmed: Boolean = false): CollectionDate {
        return CollectionDate(weekId, date,
                includeRubbish, includeRecycling, isConfirmed)
    }
}
