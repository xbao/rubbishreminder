package nz.xbc.rubbishreminder.test

import org.junit.Before
import org.robolectric.RuntimeEnvironment
import org.robolectric.shadows.ShadowLog

abstract class BaseUnitTest {

    val context = RuntimeEnvironment.application

    @Before
    fun setupBase() {
        ShadowLog.stream = System.out
    }
}
