package nz.xbc.rubbishreminder.test

import nz.xbc.rubbishreminder.App
import nz.xbc.rubbishreminder.common.di.AppComponent

class TestApplication : App() {

    override fun createAppComponent(): AppComponent {
        return super.createAppComponent()
    }

    override fun setupTimber(appComponent: AppComponent) {
        // do nothing
    }
}
