package nz.xbc.rubbishreminder.test.factory

import nz.xbc.rubbishreminder.collection.CollectionSchedule
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.util.Dates
import org.joda.time.DateTime

object CollectionScheduleFactory {
    fun createSchedule(id: Long = 0,
                       day: Day = Day.MONDAY,
                       hasRecycling: Boolean = true,
                       hasRubbish: Boolean = true,
                       recyclingDate: DateTime? = Dates.easyDate("Mon 01 Jan 2018"),
                       addressKey: String? = null) : CollectionSchedule {
        return CollectionSchedule(
                id = id,
                day = day,
                hasRecycling = hasRecycling,
                hasRubbish = hasRubbish,
                recyclingDate = recyclingDate,
                addressKey = addressKey
        )
    }
}
