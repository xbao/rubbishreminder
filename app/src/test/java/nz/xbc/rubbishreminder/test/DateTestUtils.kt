package nz.xbc.rubbishreminder.test

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object DateTestUtils {

    private val format = DateTimeFormat.forPattern("E dd MMM yyyy")
    private val formatTime = DateTimeFormat.forPattern("HH:mm E dd MMM yyyy")

    fun date(date: String) : DateTime {
        return format.parseDateTime(date)
    }
    fun dateTime(date: String) : DateTime {
        return formatTime.parseDateTime(date)
    }
}
