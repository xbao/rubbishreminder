package nz.xbc.rubbishreminder.addschedule

import com.nhaarman.mockito_kotlin.mock
import nz.xbc.rubbishreminder.common.Day
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.properties.Delegates

class AddScheduleViewModelTest {
    val format = DateTimeFormat.forPattern("E dd MMM yyyy")

    var viewModel : AddScheduleViewModel by Delegates.notNull()
    @Before
    fun setup() {
        viewModel = AddScheduleViewModel(mock());
    }
    @Test
    fun calculateRecyclingOptions_tuesdayPickedOnMonday() {
        val monday = format.parseDateTime("Mon 25 Dec 2017")
        val options = viewModel.calculateRecyclingOptions(Day.TUESDAY, monday)
        options[0].let {
            assertEquals(it.relativeDate, AddScheduleViewModel.RelativeDate.Last(Day.TUESDAY))
            assertEquals(it.date, monday.minusDays(6))
        }
        options[1].let {
            assertEquals(it.relativeDate, AddScheduleViewModel.RelativeDate.Tomorrow)
            assertEquals(it.date, monday.plusDays(1))
        }
    }
    @Test
    fun calculateRecyclingOptions_sundayPickedOnMonday() {
        val monday = format.parseDateTime("Mon 25 Dec 2017")
        val options = viewModel.calculateRecyclingOptions(Day.SUNDAY, monday)
        options[0].let {
            assertEquals(AddScheduleViewModel.RelativeDate.Yesterday, it.relativeDate)
            assertEquals(monday.minusDays(1), it.date)
        }
        options[1].let {
            assertEquals(AddScheduleViewModel.RelativeDate.Next(Day.SUNDAY), it.relativeDate)
            assertEquals(monday.plusDays(6), it.date)
        }
    }
    @Test
    fun calculateRecyclingOptions_mondayPickedOnMonday() {
        val mondayDateTime = DateTime.now().dayOfWeek().setCopy(Day.MONDAY.constant)
        val options = viewModel.calculateRecyclingOptions(Day.MONDAY, mondayDateTime)
        options[0].let {
            assertEquals(AddScheduleViewModel.RelativeDate.Last(Day.MONDAY), it.relativeDate)
            assertEquals(it.date, mondayDateTime.minusDays(7))
        }
        options[1].let {
            assertEquals(it.relativeDate, AddScheduleViewModel.RelativeDate.Today)
            assertEquals(it.date, mondayDateTime)
        }
    }
    @Test
    fun calculateRecyclingOptions_wednesdayPickedOnMonday() {
        val mondayDateTime = DateTime.now().dayOfWeek().setCopy(Day.MONDAY.constant)
        val options = viewModel.calculateRecyclingOptions(Day.WEDNESDAY, mondayDateTime)
        options[0].let {
            assertEquals(it.relativeDate, AddScheduleViewModel.RelativeDate.Last(Day.WEDNESDAY))
            assertEquals(it.date, mondayDateTime.minusDays(5))
        }
        options[1].let {
            assertEquals(it.relativeDate, AddScheduleViewModel.RelativeDate.Next(Day.WEDNESDAY))
            assertEquals(it.date, mondayDateTime.plusDays(2))
        }
    }

}
