package nz.xbc.rubbishreminder.db

import androidx.room.Room
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.test.BaseUnitTest
import nz.xbc.rubbishreminder.test.DateTestUtils.date
import nz.xbc.rubbishreminder.test.RxTestUtils
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import kotlin.properties.Delegates


@RunWith(RobolectricTestRunner::class)
class CollectionScheduleDaoUnitTest : BaseUnitTest() {

    var db: AppDatabase by Delegates.notNull()
    var dao: CollectionScheduleDao by Delegates.notNull()
    @Before
    fun setup() {
        RxTestUtils.makeRxSynchronous()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        dao = db.schedulesDao()
    }

    @After
    fun teardown() {
        db.close()
    }

    @Test
    fun insertCurrentSchedule() {
        val scheduleEntity = CollectionScheduleEntity(2, null, Day.MONDAY.constant, true, true, date("Mon 08 Jan 2018").millis)
        val id = dao.insertSchedule(scheduleEntity)

        dao.insertCurrentSchedule(CurrentScheduleEntity(id))

        val currentSchedule = dao.loadCurrentSchedule()
        assertNotNull(currentSchedule)
        currentSchedule!!
        assertEquals(scheduleEntity, currentSchedule)
    }

    @Test
    fun insertCurrentSchedule_scheduleExists_scheduleIsOverwritten() {
        val firstScheduleEntity = CollectionScheduleEntity(2, null, Day.MONDAY.constant, true, true, date("Mon 08 Jan 2018").millis)
        val secondScheduleEntity = CollectionScheduleEntity(3, null, Day.TUESDAY.constant, true, true, date("Tue 09 Jan 2018").millis)
        dao.insertSchedules(firstScheduleEntity, secondScheduleEntity)

        dao.insertCurrentSchedule(CurrentScheduleEntity(2))

        assertEquals(firstScheduleEntity, dao.loadCurrentSchedule())

        dao.insertCurrentSchedule(CurrentScheduleEntity(3))

        assertEquals(secondScheduleEntity, dao.loadCurrentSchedule())

    }
}
