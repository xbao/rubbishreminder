package nz.xbc.rubbishreminder.collection

import nz.xbc.rubbishreminder.common.Day
import nz.xbc.rubbishreminder.test.BaseUnitTest
import nz.xbc.rubbishreminder.test.factory.CollectionDateFactory.createCollectionDate
import nz.xbc.rubbishreminder.test.factory.CollectionScheduleFactory.createSchedule
import nz.xbc.rubbishreminder.util.Dates.easyDate
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class CollectionDateGeneratorTest : BaseUnitTest() {
    @Test
    fun testGenerateDates() {
        val oldest = createCollectionDate(date = easyDate("Mon 01 Jan 2018"), includeRecycling = true)
        val schedule = createSchedule(recyclingDate = oldest.date)

        val dates = CollectionDateGenerator.generateDates(2,
                oldest,
                schedule)
        assertEquals(2, dates.size)
        dates.forEach {
            assertTrue(it.includeRubbish)
            assertFalse(it.isConfirmed)
        }
        dates[0].let {
            assertEquals(easyDate("Mon 08 Jan 2018"), it.date)
            assertFalse(it.includeRecycling)
            assertEquals(oldest.weekId + 1, it.weekId)
        }
        dates[1].let {
            assertEquals(easyDate("Mon 15 Jan 2018"), it.date)
            assertTrue(it.includeRecycling)
            assertEquals(oldest.weekId + 2, it.weekId)
        }
    }
    @Test
    fun testGenerateDates_scheduleDayAndOldestDayDiffer() {
        val oldest = createCollectionDate(
                date = easyDate("Tue 16 Jan 2018"),
                includeRubbish = true,
                includeRecycling = true)
        val schedule = createSchedule(
                day = Day.MONDAY,
                hasRubbish = true,
                hasRecycling = true,
                recyclingDate = easyDate("Mon 01 Jan 2018"))

        val dates = CollectionDateGenerator.generateDates(2,
                oldest,
                schedule)
        assertEquals(2, dates.size)
        dates.forEach {
            assertTrue(it.includeRubbish)
            assertFalse(it.isConfirmed)
        }
        dates[0].let {
            assertEquals(easyDate("Mon 22 Jan 2018"), it.date)
            assertFalse(it.includeRecycling)
            assertEquals(oldest.weekId + 1, it.weekId)
        }
        dates[1].let {
            assertEquals(easyDate("Mon 29 Jan 2018"), it.date)
            assertTrue(it.includeRecycling)
            assertEquals(oldest.weekId + 2, it.weekId)
        }
    }

}
