package nz.xbc.rubbishreminder.alarm

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.subjects.PublishSubject
import nz.xbc.rubbishreminder.collection.CollectionDate
import nz.xbc.rubbishreminder.collection.CollectionRepo
import nz.xbc.rubbishreminder.test.BaseUnitTest
import nz.xbc.rubbishreminder.test.DateTestUtils.dateTime
import nz.xbc.rubbishreminder.test.RxTestUtils
import nz.xbc.rubbishreminder.test.factory.CollectionDateFactory.createCollectionDate
import nz.xbc.rubbishreminder.util.Dates.easyDate
import org.joda.time.DateTimeUtils
import org.junit.After
import org.junit.Before
import org.junit.Test

class ReminderManagerUnitTest : BaseUnitTest() {

    lateinit var reminderManager: ReminderManager
    lateinit var mockAlarmManager: AlarmManagerFacade
    lateinit var collectionDatesSubject: PublishSubject<List<CollectionDate>>

    @Before
    fun setup() {
        RxTestUtils.makeRxSynchronous()
        collectionDatesSubject = PublishSubject.create<List<CollectionDate>>()
        val mockCollectionRepo: CollectionRepo = mock()
        whenever(mockCollectionRepo.observeCollectionDates()).thenReturn(collectionDatesSubject)

        mockAlarmManager = mock()

        reminderManager = ReminderManager(mockCollectionRepo, mockAlarmManager)
        reminderManager.startObservingDates()
    }

    @After
    fun teardown() {
        reminderManager.stopObservingDates()
    }

    @Test
    fun setNextAlarm() {
        val nextDate = createCollectionDate(easyDate("Mon 8 Jan 2018"), true, true)
        // Set the time to be before the next date, so that the ReminderManager will allow it as an alarm
        DateTimeUtils.setCurrentMillisFixed(easyDate("Sat 6 Jan 2018").millis)
        val expectedAlarmDate = dateTime("18:00 Sun 7 Jan 2018");
        collectionDatesSubject.onNext(listOf(
                nextDate,
                createCollectionDate(easyDate("Mon 15 Jan 2018"), true, false)
        ))

        verify(mockAlarmManager).setAlarm(expectedAlarmDate.millis, nextDate)
        DateTimeUtils.setCurrentMillisSystem()
    }

}
