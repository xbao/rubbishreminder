package nz.xbc.rubbishreminder


import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import nz.xbc.multiadapter.MultiAdapter
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class MultiAdapterHelperTest {
    var helper = MultiAdapter.MultiAdapterHelper()
    @Before
    fun setup() {
        helper = MultiAdapter.MultiAdapterHelper()
    }

    @Test
    fun adjustItemPosition_multipleAdapters() {
        helper.add(createAdapter(1))
        helper.add(createAdapter(4))
        helper.add(createAdapter(2))
        Assert.assertEquals(0, helper.adjustItemPosition(1))
        Assert.assertEquals(2, helper.adjustItemPosition(3))
        Assert.assertEquals(0, helper.adjustItemPosition(5))
    }

    @Test
    fun getItemCount_singleAdapter() {
        helper.add(createAdapter(2))

        Assert.assertEquals(2, helper.getItemCount())
    }

    @Test
    fun getItemCount_twoAdapters() {
        val helper = MultiAdapter.MultiAdapterHelper()
        helper.add(createAdapter(2))
        helper.add(createAdapter(4))

        Assert.assertEquals(6, helper.getItemCount())
    }

    @Test
    fun getSubadapterFromItemPosition() {
        val adapter1 = createAdapter(2)
        val adapter2 = createAdapter(4)
        val adapter3 = createAdapter(3)
        helper.add(adapter1)
        helper.add(adapter2)
        helper.add(adapter3)
        Assert.assertEquals(adapter1, helper.getSubadapterFromItemPosition(1))
        Assert.assertEquals(adapter2, helper.getSubadapterFromItemPosition(2))
        Assert.assertEquals(adapter2, helper.getSubadapterFromItemPosition(5))
        Assert.assertEquals(adapter3, helper.getSubadapterFromItemPosition(6))
        Assert.assertEquals(adapter3, helper.getSubadapterFromItemPosition(8))
    }

    @Test
    fun addAndGetIndexFromViewType() {
        val mungedViewType = helper.addIndexToViewType(5, 1)
        Assert.assertEquals(1, helper.removeIndexFromViewType(mungedViewType))
        Assert.assertEquals(5, helper.getIndexFromViewType(mungedViewType))
    }

    @Test
    fun addAndGetIndexFromViewType_limits() {
        val adapterIndex = 0xFF
        val viewType = 0x00FF_FFFF
        val mungedViewType = helper.addIndexToViewType(adapterIndex, viewType)
        Assert.assertEquals(viewType, helper.removeIndexFromViewType(mungedViewType))
        Assert.assertEquals(adapterIndex, helper.getIndexFromViewType(mungedViewType))
    }


    companion object {
        fun createAdapter(itemCount: Int): TestAdapter {
            return object : TestAdapter() {
                override fun getItemCount(): Int {
                    return itemCount
                }
            }
        }
    }

    abstract class TestAdapter : MultiAdapter.Subadapter<TestViewHolder>() {
        override fun getItemCount(): Int {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

    class TestViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
}
