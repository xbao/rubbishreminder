package nz.xbc.rubbishreminder.common

import org.joda.time.DateTimeConstants
import org.joda.time.format.DateTimeFormat
import org.junit.Assert.assertEquals
import org.junit.Test

class DateExtensionsKtTest {
    val format = DateTimeFormat.forPattern("E dd MMM yyyy")
    @Test
    fun withPreviousDayOfWeek_mondayOnSunday_sameWeek() {
        val current = format.parseDateTime("Sun 24 Dec 2017")
        val expected = format.parseDateTime("Mon 18 Dec 2017")

        assertEquals(expected, current.withPreviousDayOfWeek(DateTimeConstants.MONDAY))
    }

    @Test
    fun withPreviousDayOfWeek_sundayOnMonday_previousWeek() {
        val current = format.parseDateTime("Mon 25 Dec 2017")
        val expected = format.parseDateTime("Sun 24 Dec 2017")

        assertEquals(expected, current.withPreviousDayOfWeek(DateTimeConstants.SUNDAY))
    }

    @Test
    fun withPreviousDayOfWeek_thursdayOnThursday_previousWeek() {
        val current = format.parseDateTime("Thu 21 Dec 2017")
        val expected = format.parseDateTime("Thu 14 Dec 2017")

        assertEquals(expected, current.withPreviousDayOfWeek(DateTimeConstants.THURSDAY))
    }
}
