package nz.xbc.rubbishreminder.common

import nz.xbc.rubbishreminder.common.WeekIds.convertToWeekId
import nz.xbc.rubbishreminder.util.Dates
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals


class WeekIdsTest {
    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testConvertToWeekId() {
        assertEquals(0, convertToWeekId(Dates.easyDate("Tue 03 Jan 2017")))
        assertEquals(0, convertToWeekId(Dates.easyDate("Sun 08 Jan 2017")))
        assertEquals(1, convertToWeekId(Dates.easyDate("Mon 09 Jan 2017")))
    }

    @Test
    fun testConvertToWeekId_2018() {
        assertEquals(57, convertToWeekId(Dates.easyDate("Thu 08 Feb 2018")))
    }
}
